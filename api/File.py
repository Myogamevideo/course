import inspect
import json
import datetime
import os
import requests

class File:
    @staticmethod
    def save_products(data) -> None:
        name_product = data.name_product
        file_path = f'var/product/{name_product}.json'

        if os.path.exists(file_path):
            os.remove(file_path)
        else:
            File.save_log("The file does not exist")

        json_data = {
            "name_product": name_product,
            "len_products": data.len_products,
            "min_prices": data.min_prices,
            "avg_prices": data.avg_prices,
            "median_prices": data.median_prices,
            "promos": data.promos,
            "products": [
                {
                    "name": product.name,
                    "description": product.description,
                    "price_kg": product.price_kg,
                    "price": product.price,
                    "discount": product.discount,
                    "market": product.market,
                    "picture": product.picture,
                    "link_product": product.link_product
                }
                for product in data.products
            ],
            "list_store_carts": [
                {
                    "name_product": store_cart.name_product,
                    "name_store": store_cart.name_store,
                    "len_products": store_cart.len_products,
                    "min_prices": store_cart.min_prices,
                    "avg_prices": store_cart.avg_prices,
                    "median_prices": store_cart.median_prices,
                    "products": [
                        {
                            "name": product.name,
                            "description": product.description,
                            "price_kg": product.price_kg,
                            "price": product.price,
                            "discount": product.discount,
                            "market": product.market,
                            "picture": product.picture,
                            "link_product": product.link_product
                        }
                        for product in store_cart.products
                    ]
                } for store_cart in data.list_store_carts
            ]
        }
        
        with open(file_path, "w") as json_file:
            json.dump(json_data, json_file, indent=4)

    @staticmethod
    def read_simple_file(name_file) -> list:
        datas = []
        try:
            with open('var/cookie/' + name_file, 'r') as f:
                data_list = json.load(f)
                for data in data_list:
                    datas.append(data)
        except FileNotFoundError:
            File.save_log("FileNotFoundError")

        return datas

    @staticmethod
    def save_simple_file(name_file, datas) -> None:
        new_datas = []
        for data in datas:
            if (type(datas) == list and data['final_price'] is None):
                new_datas.append(data.replace(' ', '+'))
            else:
                new_datas = datas
        with open(name_file, 'w') as f:
            json.dump(new_datas, f)
        File.save_log(
            f"Les données ont été enregistrées dans le fichier {name_file}.")

    @staticmethod
    def save_log(message) -> None:
        print(message)

        frame = inspect.stack()[1]
        file_name = frame[0].f_code.co_filename
        line_number = frame[0].f_lineno

        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        log_entry = f"[{timestamp}] {message} ({file_name}:{line_number})\n"
        day = datetime.datetime.now().strftime("%Y-%m-%d")
        logname = f"var/log/[{day}]-log.txt"

        with open(logname, 'a') as f:
            f.write(log_entry)
