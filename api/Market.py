from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
import re
import threading
import requests
import json
from selenium import webdriver
from File import File
from Product import Product
from StoreCart import StoreCart
from bs4 import BeautifulSoup


class Market:
    results = []

    def __init__(self) -> None:
        pass

    @staticmethod
    def run(drivers, name_product, index, code_postal):
        Market.results = []
        threads = []

        for name_driver in drivers:
            threads.append(threading.Thread(target=eval('Market.get' + name_driver),
                           args=(drivers[name_driver], index, name_product, code_postal)))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        return Market.results

    @staticmethod
    def remove_char_price(price_kg):
        non_numeric_chars = [' ', 'les2', '€', 'KG', 'kg', 'L', 'e=', 'l', '(', ')', 'kio',
                             'itre', '/', 'Kiogramme', 'unité', '\'', 'ekio', 'U', 'p', 'ce']
        for char in non_numeric_chars:
            price_kg = price_kg.replace(char, '')
            price_kg = price_kg.replace(',', '.')
        
        if not price_kg or price_kg == '.' or 'pce' in price_kg:
            return ''
        
        return float(price_kg)

    def getAction(driver, index, name_product, code_postal):
        url = ""
        headers = {}
        response = requests.get(url, headers=headers)
        results = []
        if response.status_code == 200:
            description = ''
        sc = StoreCart(name_product, 'Action', results)
        Market.results.append(sc)

    def getCourseU(driver, index, name_product, code_postal):
        url = "https://www.coursesu.com/recherche?q=" + name_product
        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "gzip, deflate, br",
            "Referer": "https://www.coursesu.com",
            "Connection": "keep-alive",
            "Cookie": "newsletterDisplayNb=0; dwanonymous_8ce4ace52d14ef4ce78780bbb2b979c7=abureQ5Uo76V4V2ts8ZAzW1N9o; datadome=0_uAnlOIUl8Vo7JdzVvFOHeo_p0SGj5hCk2M8HijH9Z3RgZQNk7SYxidTV6Wc0n9pGGqxgoCQwUpRewiikii-MOSATLDAXKFLAdh~B1iJqcPZhiFSxA09jnFMA6~NH9p; TCPID=12334856334616342058; TC_PRIVACY=0%40007%7C14%7C3692%401%2C2%2C3%40%401677743799804%2C1677743799804%2C1693295799804%40; TC_PRIVACY_CENTER=1%2C2%2C3; mbox=PC#c2ca55a9e8e44f04b0480e9c0f2a40fd.37_0#1740988601|session#208f21283c934049bdef0c6c2ffe6adb#1678707219; _sfid_045b={%22anonymousId%22:%228905f54dc802f260%22%2C%22consents%22:[{%22consent%22:{%22purpose%22:%22Personalization%22%2C%22provider%22:%22TagCommander%22%2C%22status%22:%22Opt%20In%22}%2C%22lastUpdateTime%22:%222023-03-02T07:56:40.142Z%22%2C%22lastSentTime%22:%222023-03-02T07:56:40.254Z%22}]}; _evga_474d={%22uuid%22:%228905f54dc802f260%22}; AMCV_9300575E557F044A7F000101%40AdobeOrg=1585540135%7CMCMID%7C02989718639727001583440410519592610055%7CMCAAMLH-1679310157%7C6%7CMCAAMB-1679310157%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1678712557s%7CNONE%7CMCSYNCSOP%7C411-19434%7CvVersion%7C4.4.0; _cs_c=0; _cs_id=0bf45162-3402-a57f-8416-f56c3a752feb.1677743800.9.1678705357.1678705357.1.1711907800204; _gcl_au=1.1.1644489242.1677743800; _lm_id=1PXAR3BUKWTHJ1L1; aam_uuid=02995763771387584503436437077045818827; cto_bundle=zNMoal9RMm9pVDd0NVlLQ0JjRFJ1N3BPa3ZmWHZCbE03QlBJeXREejhucllGbnNiNiUyRnRjVk42b0FEaWFBOGxjR3NwOGV5Y0Z0Q1QyalJXbVdNNFF2S0RnaEklMkJFJTJCTFJBdmpsNHc5TFVrZFFycUpYaVhvVW9TaTBzS3h6bGc2ZWQxbUtmTVBUMExFS3ZyTnVET1MlMkZQN05ZRE15QSUzRCUzRA; dwpersonalization_8ce4ace52d14ef4ce78780bbb2b979c7=4965c6f68e320bc667441bc80620230313224500000; tCdebugLib=1; storeId=68553; __cq_uuid=abureQ5Uo76V4V2ts8ZAzW1N9o; __cq_seg=0~0.00!1~0.00!2~0.00!3~0.00!4~0.00!5~0.00!6~0.00!7~0.00!8~0.00!9~0.00; _scid=6ec43454-51b8-4a8a-9191-90e4b968c2a6; _tt_enable_cookie=1; _ttp=lgPHquIY3Q8Qu868E955uD7kvHK; _hjSessionUser_3357742=eyJpZCI6IjUxZWUwZTFjLTM3NDQtNTAxNi1iNWQ3LWVmODQ3MzVlYzMzMiIsImNyZWF0ZWQiOjE2Nzg0NjI2ODkzOTAsImV4aXN0aW5nIjp0cnVlfQ==; dwac_8d8721aecf7af8fe498c1f352b=mbEBDXYLDHpK3TRaJEiLxPNZ6j9Iej9lwSQ%3D|dw-only|||EUR|false|Europe%2FParis|true; cqcid=abureQ5Uo76V4V2ts8ZAzW1N9o; cquid=||; sid=mbEBDXYLDHpK3TRaJEiLxPNZ6j9Iej9lwSQ; __cq_dnt=0; dw_dnt=0; clickedQueries=coca; dwsid=K8ERK9TiqioHHneKNpQYw0Ep-hnqk5k1jOdEzSCp2Qx38DfvCz8Ih3Ox6G0w1oe2Su4OsKuRLbqcSEyWJScaKg==; at_check=true; dtCookie=v_4_srv_1_sn_6M5I9PVT19M0KOJ37AAP13NN588860BH_app-3A3b6379e18cf94c9d_1_ol_0_perc_100000_mul_1; rxVisitor=1678696503600990K0NRN3777OAGM1NQOCOI1E42G9BQC; dtPC=1$505356826_32h-vJHVRBDTRQKWOUMKMVMQKHFKAHUGCCTPG-0e0; rxvt=1678707160267|1678703720430; dtLatC=117; dtSa=false%7C_load_%7C14%7C_onload_%7C-%7C1678705360234%7C505356826_32%7Chttps%3A%2F%2Fwww.coursesu.com%2Frecherche%3Fq%3Dcoca%7C%7C%7C%7C; KPVAL_DFS=20px; KPVAL_DWW=700px; cpc_bing=true; AMCVS_9300575E557F044A7F000101%40AdobeOrg=1; s_cc=true; redirectToStore=true; _hjIncludedInSessionSample_3357742=0; _hjSession_3357742=eyJpZCI6IjZmZDA0YTEyLTRlYjktNDk3YS1hZWY4LWI2MmYyNzdiYzg4MSIsImNyZWF0ZWQiOjE2Nzg3MDUzNTY4OTUsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=0; _uetsid=f612b480c17911ed81d15187429ae291; _uetvid=c3e27bd0b8cf11ed8e167940a977da9f;",
            "Upgrade-Insecure-Requests": "1",
            "Sec-Fetch-Dest": "document",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "same-origin",
            "Sec-Fetch-User": "?1",
            "Sec-GPC": "1",
            "TE": "trailers"
        }
        response = requests.get(url, headers=headers)
        results = []
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')
            products = soup.find_all(class_='product-tile')
            for product in products:
                name = product.find(class_='name-link').text.replace('\n', '')
                description = ''
                if product.find(class_='unit-info') is not None:
                    price_kg = Market.remove_char_price(
                        product.find(class_='unit-info').text)
                else:
                    price_kg = ''

                if product.find(class_='sale-price') is not None:
                    price = Market.remove_char_price(
                        product.find(class_='sale-price').text)
                else:
                    price = ''
                discount = product.find(
                    class_='promotion-block-info').text if product.find(class_='promotion-block-info') else ''
                link = 'https://www.coursesu.com' + \
                    product.find(class_='product-tile-link').get('href')
                picture = product.find(
                    class_='primary-image')['data-src'].split(' ')[0]
                p = Product(name, description, price_kg, price,
                            discount, 'CourseU', picture, link)
                results.append(p)
        sc = StoreCart(name_product, 'CourseU', results)
        Market.results.append(sc)

    def getLeclerc(driver, index, name_product, code_postal):
        url = "https://fd14-courses.leclercdrive.fr/magasin-016301-Clermont-Ferrand/recherche.aspx?TexteRecherche="+name_product
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Referer': 'https://fd14-courses.leclercdrive.fr',
            'Cookie': 'clsWCSD333:iDRIVE31=null; datadome=52xaDhkXx7YY9zr5hVsGkdWPrq~22w0gkfOwDUzdQJkQU6jL-Ho0Yuy2rAV3ZL8-bg7iFpmVQ1m5-X27J1yrh6VEb2m04K71-E71HQlV2SwNr0u-4viP99Gy-xMLazGL; MacId=id=33d57f30-0ed5-455b-9eb8-bbf129758302; OptanonConsent=isGpcEnabled=1&datestamp=Mon+Mar+13+2023+18%3A08%3A38+GMT%2B0100+(heure+normale+d%E2%80%99Europe+centrale)&version=6.24.0&isIABGlobal=false&hosts=&genVendors=&consentId=1e7b725d-f835-4f85-83d1-c28af7926ee5&interactionCount=1&landingPath=NotLandingPage&groups=1%3A1%2C2%3A1%2C3%3A1%2C4%3A1&geolocation=%3B&AwaitingReconsent=false; OptanonAlertBoxClosed=2023-02-28T15:27:14.015Z; C360i=2D3E79CBC4054E83D829661C953A3D19|eyJjcmVhdGVkIjoxNjc3NTk4MDM0MzY5LCJ1cGRhdGVkIjoxNjc4NzI3MzE4ODM4LCJ0YWdfaWQiOiI0LjMuMCIsImNvdW50IjoyNSwiZXhwIjoxNzA5MTM0MDM0MzY5fQ==; _ga=GA1.2.1564504529.1677598035; clsWCSD107:ContexteMU=@d=2%7c016301; ASP.NET_SessionId=aa3igel5a5pd2fplug1ndjkc; Canal=sCANAL=SD; TagCommander=; clsWCSD045:PageCache=c=1; wdrivesr2=!cTMfqba1qjS+cXQDMS1fZ3GiHlzpRqRxUgWOb1JIYWPW4BCWZhScDIjtQxgqhgJzXHX+gj9BJPM4JA==; TS01b20143=0130c016ab067d63068014faf74ed91e2d24bec558746c72b02024cda31d999e5ca40f51af9927672335589e096825ae42b7e6c310; TS01e6e41f=0130c016abedd21e575c26375a4157f19769d1c9d8cb0a948f6ce036b9c9f93a6b34e6f8bdb196a7ad6fe7c37f9823cca2d0e93de9; TS018e2733=0130c016abedd21e575c26375a4157f19769d1c9d8cb0a948f6ce036b9c9f93a6b34e6f8bdb196a7ad6fe7c37f9823cca2d0e93de9; cdrivesr2=!j7cRynTDtEHrmone7bEI1v9taV9ugv3pseRkpMWI8YJAjvcS/7ZyDzqOGFwemD393D3rrSjrjL0C/h0=; _gid=GA1.2.1446715763.1678696505; clsWCSD333:iDRIVE31=null; _gat_UA-138466009-3=1; _uetsid=f36815d0c17911edbcfd19e06d98c1fa; _uetvid=6097ea30b77c11edbedadf2d91c8cc78',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Sec-GPC': '1'
        }
        response = requests.get(url, headers=headers)
        results = []
        if response.status_code == 200:
            data = response.text
            find_none = BeautifulSoup(response.content, 'html.parser').find(class_='divWCRS340_PasDeResultats')
            if find_none == None:
                data = data.split("lstElements", 1)[1]
                data = data.replace("\":", "", 1)
                data = data.split("},\"objContenuGroupeVariantes", 1)[0]
                data_json = json.loads(data)
                for product in data_json:
                    name = product['objElement']['sLibelleLigne1']
                    description = product['objElement']['sLibelleLigne2']
                    if product['objElement']['fAfficherPrixParUniteMesure'] != False:
                        price_kg = product['objElement']['nrPVParUniteDeMesureTTC']
                    else:
                        price_kg = ''
                    price = product['objElement']['sPrixPromo']
                    if product['objElement']['sTexteTooltipLot'] != '':
                        discount = product['objElement']['sTexteTooltipLot']
                    else:
                        discount = ''
                    link = product['objElement']['sUrlPageProduit']
                    picture = product['objElement']['sUrlVignetteProduit']
                    p = Product(name, description, price_kg, price,
                                discount, 'Leclerc', picture, link)
                    results.append(p)
        sc = StoreCart(name_product, 'Leclerc', results)
        Market.results.append(sc)

    def getIntermarche(driver, index, name_product, code_postal):
        url = "https://www.intermarche.com/api/service/produits/v2/pdvs/09708/products/byKeywordAndCategory"
        headers = {
            "Host": "www.intermarche.com",
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
            "Accept": "application/json, text/plain, */*",
            "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "gzip, deflate, br",
            "Content-Type": "application/json;charset=utf-8",
            "x-red-device": "red_fo_desktop",
            "x-red-version": "3",
            "x-optional-oauth": "true",
            "x-service-name": "produits",
            "x-itm-device-fp": "1133caf9-9da6-4d6b-9695-f419e9bc2bc8",
            "x-itm-session-id": "c7df653a-6531-4abb-bbe0-fedb5cb22fe0",
            "x-pdv": '{"ref":"09708","isEcommerce":true,"name":"Contact%20Romagnat"}',
            "itm_code": "itm-code-node",
            "isNode": "false",
            "Content-Length": "109",
            "Origin": "https://www.intermarche.com",
            "Alt-Used": "www.intermarche.com",
            "Connection": "keep-alive",
            "Referer": "https://www.intermarche.com/recherche/coca",
            "Cookie": "datadome=4lzSVVKrfjhp8RCiyJ5fG_o~wXQvzBjN8QuYfQXcZWgAcY8MuyaJVw85ymw7ixk5chCLlHCnQ~_b6XYuwHllgxJkQRzdZd9U~e_2hGlxKmPkquHaUoAyu53gTrWoV_xt; didomi_token=eyJ1c2VyX2lkIjoiMTg2OTg5YmEtZGFiMC02OTA0LTllMWItYWM1NDkxZTI2NDQ1IiwiY3JlYXRlZCI6IjIwMjMtMDItMjhUMTU6MTk6NDguMjc5WiIsInVwZGF0ZWQiOiIyMDIzLTAyLTI4VDE1OjE5OjQ4LjI3OVoiLCJ2ZW5kb3JzIjp7ImVuYWJsZWQiOlsiZ29vZ2xlIiwiYzpuZXN0bGUtUUxyVEx5OXQiLCJjOmx1Y2t5Y2FydC1MSmJQRnJTaiIsImM6YmluZy1hZHMiLCJjOm1lZGlhbm9lLThLc3BUNVFaIiwiYzpwaW50ZXJlc3QiLCJjOmFiLXRhc3R5IiwiYzpxdWFudHVtLWFkdmVydGlzaW5nIiwiYzpjb250ZW50c3F1YXJlIiwiYzp1c2FiaWxsYSIsImM6cHJvY3RlcmFuLVEzVkVKTmlZIiwiYzpnb29nbGVhbmEtckp4emNjNjMiLCJjOnNuYXBjaGF0LWZ6TlVFaXpqIiwiYzp0aWt0b2stS1pBVVFMWjkiLCJjOmRhdGFkb21lLWU2RGpnbXI3IiwiYzpkaWRvbWktVGZ4enRBejkiLCJjOmR5bmF0cmFjZS1RWUZtaVRNQyIsImM6cXVldWVpdC1XWVpmTFJ4TCIsImM6YWRvdG1vYiIsImM6bWF0Y2hhLWF5ejNCTEw5IiwiYzpydGJob3VzZS15Sll4WjkyYyJdfSwicHVycG9zZXMiOnsiZW5hYmxlZCI6WyJnZW9sb2NhdGlvbl9kYXRhIiwiZGV2aWNlX2NoYXJhY3RlcmlzdGljcyJdfSwidmVyc2lvbiI6MiwiYWMiOiJDOEdBR0FGa0Fvd0x3UUFBLkFGbUFDQUZrIn0=; _hhr=0; _gcl_au=1.1.2083151594.1677597585; itm_device_id=1133caf9-9da6-4d6b-9695-f419e9bc2bc8; itm_usid=c7df653a-6531-4abb-bbe0-fedb5cb22fe0; euconsent-v2=CPn5KgAPn5KgAAHABBENC5CsAP_AAE7AAAAAF5wDAAKgAZAA3AB8AIAAeACEAFIAMYAcQBEwCOALzAAAAOKgAwABBKEpABgACCUJKADAAEEoR0AGAAIJQkIAMAAQShCQAYAAglCMgAwABBKE.f_gACdgAAAAA; lstordrdt=0; _ga_3YXL8ZKM7H=GS1.1.1678457881.8.1.1678459246.0.0.0; _ga=GA1.2.1452208591.1677597589; ABTasty=uid=sxwt8abzk00apsvz&fst=1677597588703&pst=1678437958290&cst=1678457881616&ns=9&pvt=21&pvis=6&th=; _cs_c=1; _cs_id=72346795-3402-a559-8229-015bcbf7a5ef.1677597588.10.1678459247.1678457881.1.1711761588815; _scid=c9c548d4-fabd-44ef-bf40-d25a66b16d27; cjConsent=MHxZfDB8Tnww; _tt_enable_cookie=1; _ttp=XPt8y7p8wF4xQhaR3zm5emQF2EF; spdv=1; itm_pdv={%22ref%22:%2209708%22%2C%22isEcommerce%22:true%2C%22name%22:%22Contact%2520Romagnat%22}; novaParams={%22pdvRef%22:%2209708%22}; cikneeto_uuid=id:78b6958c-5c85-4537-9a4d-d3fb064119a7; _pin_unauth=dWlkPU5EWXdOREUyTXpJdFptUTNNeTAwTnpoa0xUZzBPR1F0WlRrMU5XTTJaV0U0TXpVNA; _gid=GA1.2.382889161.1678305492; cikneeto=date:1678459335232; _cs_mk_ga=0.5878983370066552_1678457881534; ABTastySession=mrasn=&lp=https%253A%252F%252Fwww.intermarche.com%252Frecherche%252Fchocapic; _cs_s=5.5.0.1678461047404; QueueITAccepted-SDFrts345E-V3_eventproditm=EventId%3Deventproditm%26QueueId%3D00000000-0000-0000-0000-000000000000%26RedirectType%3Ddisabled%26IssueTime%3D1678457882%26Hash%3D1a8860d608db5bbb3cabd0a0a2e9834adb2e3a09373e6e6261499ca2b924e8d8; _gcl_aw=GCL.1678458051.Cj0KCQiAx6ugBhCcARIsAGNmMbj2zZEepLxcCbzG7OWrDrx9GtZ27eVwQuL0p021KX3xjkiiOZc2WSAaArRCEALw_wcB; _gcl_dc=GCL.1678458051.Cj0KCQiAx6ugBhCcARIsAGNmMbj2zZEepLxcCbzG7OWrDrx9GtZ27eVwQuL0p021KX3xjkiiOZc2WSAaArRCEALw_wcB; _gac_UA-140041241-1=1.1678458052.Cj0KCQiAx6ugBhCcARIsAGNmMbj2zZEepLxcCbzG7OWrDrx9GtZ27eVwQuL0p021KX3xjkiiOZc2WSAaArRCEALw_wcB; _uetsid=8def2670bdeb11ed8f5c27fd20096ef2; _uetvid=57195ad0b77b11edbdc0453a948ceb44",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin",
            "Sec-GPC": "1",
            "TE": "trailers"
        }
        data = {
            "keyword": name_product,
            "page": 1,
            "size": 20,
            "filtres": [],
            "tri": "pertinence",
            "ordreTri": None,
            "catalog": ["PDV"]
        }
        response = requests.post(url, headers=headers, data=json.dumps(data))
        results = []
        if response.status_code == 200:
            datas = response.json()
            products = datas['produits']
            for product in products:
                name = product['libelle']
                description = product['conditionnement'] if product['conditionnement'] is not None else ''
                price_kg = product['unitPrice']
                price = product['prix']
                try:
                    if len(product['avantages']) > 0:
                        discount = str(product['avantages'][0]['valeur']) + product['avantages'][0]['type'] + \
                            ' remise : ' + \
                            str(product['avantages'][0]['remise'])
                    else:
                        discount = ''
                except:
                    discount = ''
                picture = product['images'][0]
                link = ''
                p = Product(name, description, price_kg, price,
                            discount, 'Intermarche', picture, link)
                results.append(p)
        sc = StoreCart(name_product, 'Intermarche', results)
        Market.results.append(sc)

    def getMonoprix(driver, index, name_product, code_postal):
        driver.get("https://courses.monoprix.fr/products/search?q=" + name_product)
        if index == 0:
            try:
                onetrust = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'onetrust-accept-btn-handler')))
                driver.execute_script("arguments[0].click();", onetrust)
            except TimeoutException:
                File.save_log("Monoprix : Loading took too much time !")
        time.sleep(2)
        results = Market.getMarket(
            'Monoprix',
            driver,
            'base__BoxCard-sc-1mnb0pd-5',
            'base__Title-sc-1mnb0pd-27',
            'base__SizeText-sc-1mnb0pd-38',
            '.fop__PricePerText-sc-sgv9y1-5',
            'base__Price-sc-1mnb0pd-29',
            'fop__OfferBoxContainer-sc-sgv9y1-10',
            '.base__Media-sc-1mnb0pd-12 img',
            '.base__Media-sc-1mnb0pd-12 a',
        )
        sc = StoreCart(name_product, 'Monoprix', results)
        Market.results.append(sc)

    def getCora(driver, index, name_product, code_postal):
        if index == 0:
            try:
                driver.get(
                    "https://www.cora.fr/recherche?keywords=" + name_product)
                onetrust = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, 'onetrust-accept-btn-handler')))
                driver.execute_script("arguments[0].click();", onetrust)
                thumbnail__see = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                    (By.CLASS_NAME, 'c-product-list-item__button-other-product')))
                driver.execute_script("arguments[0].click();", thumbnail__see)
                query = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.NAME, 'searchfield')))
                query.send_keys(code_postal, Keys.ENTER)
                search_sugest = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'c-storelocator__set-shop')))
                driver.execute_script("arguments[0].click();", search_sugest)
                time.sleep(2)
            except TimeoutException:
                File.save_log("Cora : Loading took too much time !")

        driver.get("https://www.cora.fr/recherche?keywords=" + name_product)
        time.sleep(2)
        results = Market.getMarket(
            'Cora',
            driver,
            "c-product-list-item",
            "c-product-list-item--grid__title",
            '',
            '.c-product-list-item__unit-price-em',
            'c-price__amount',
            'u-text-ellipsis',
            '.c-product-image__main-image img',
            ''
        )
        sc = StoreCart(name_product, 'Cora', results)
        Market.results.append(sc)

    def getAldi(driver, index, name_product, code_postal):
        driver.get("https://www.aldi.fr/recherche.html?query=" + name_product)
        time.sleep(2)
        results = Market.getMarket(
            'Aldi',
            driver,
            "mod-article-tile",
            "mod-article-tile__title",
            'price__unit',
            '.price__base',
            'price__wrapper',
            'price__previous-percentage',
            '.mod-article-tile__media img',
            '.mod-article-tile__action'
        )
        sc = StoreCart(name_product, 'Aldi', results)
        Market.results.append(sc)

    def getCasino(driver, index, name_product, code_postal):
        if index == 0:
            try:
                driver.get(
                    "https://www.casino.fr/ecommerce/z_catalog/rechercheNormaleResultat.do?query=" + name_product)
                # onetrust = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'onetrust-accept-btn-handler')))
                # driver.execute_script("arguments[0].click();", onetrust)
                query = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'input-text-custom__input')))
                print('work')
                query.send_keys(code_postal, Keys.ENTER)
                block_store = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'block-store__main-link')))
                driver.execute_script("arguments[0].click();", block_store)
                lightbox_close = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'lightbox-close')))
                driver.execute_script("arguments[0].click();", lightbox_close)
            except TimeoutException:
                File.save_log("Casino : Loading took too much time !")
        driver.get(
            "https://www.casino.fr/ecommerce/z_catalog/rechercheNormaleResultat.do?query=" + name_product)
        time.sleep(2)
        results = Market.getMarket(
            'Casino',
            driver,
            "product-item",
            "product-item__description",
            '',
            '.product-item__conditioning',
            'product-item__offer-price',
            '.product-item__top-pictos img',
            '.product-item__img-link img',
            '.product-item__description'
        )
        driver.minimize_window()
        sc = StoreCart(name_product, 'Casino', results)
        Market.results.append(sc)

    def getCarrefour(driver, index, name_product, code_postal):
        url = "https://www.carrefour.fr/s?q=" + name_product
        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "gzip, deflate, br",
            "Upgrade-Insecure-Requests": "1",
            "Sec-Fetch-Dest": "document",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "same-origin",
            "Sec-GPC": "1",
            "Connection": "keep-alive",
            "Cookie": "__cfwaitingroom=ChhTYnpYa3hSaUVvdDY1WmdwK1dQK2FnPT0SlAIrVnZrY29kRk05aTdscDBZOTFXaVpQUXlmRGJEN25pU3lzMjgrMTNYSE9RUmM3S01ua240TFFyNGkzUzFhdk5oQ2tOSERzMnBnVmVHKzU2R0k5akl5bm1CZ3FRNEExWC95dTQrN0dYU0hxcEVYa1pxZFpoUXlUeU1qOXRZNVZ6Yk93ejkxTUt4Q1RWRlBiaG44Z1hGVzB6cnM0OGNsWm5xbEkyZkNjR0ttSExPOHo3VkpjQWE5ejZEcThVd2p2V2ttMUhURUNod3NqRVVtVEZWdHlSZjAzMTkvaHQ3d0pPVmlqTGlkWnFvU05udjcxdUVPRi9GT1BpTno3N3lSTWc3MFcxVHhkYXR5M1BEU3p5Tm5wbz0%3D; __cf_bm=CHK70e3dk46tX2.ahWYv5C0CmJKOLI2Z074v_48UOgU-1678913635-0-ARpwgiF/ZBjD0SG5qee...1%3A0%2CC0035%3A0%2CC0034%3A0%2CC0063%3A0%2CC0157%3A0%2CC0003%3A0%2CC0135%3A0%2CC0136%3A0%2CC0007%3A0%2CSTACK1%3A0%2CSTACK42%3A0&AwaitingReconsent=false; aaaaaaaaa944fac35b02f4d9a99619247b88ad463_cs_nt=MjM1NWMyMTItZWMzZC00YjBkLWJkODYtYTY4NmM2YjIxYjk1JGJleWNzJDRlOWUxNGRlLTVmNjAtNDhmOC1hNTEwLTBiNDllYzYwMDBlMiRiZXljcyQ5MDA2MmExMC02YTExLTRmYzQtOTY0My02MWVmYmEwMTEzZGI=; _ga_S8S9P3NTNV=GS1.2.1678913521.24.1.1678913637.60.0.0; tc_cj_v2=%5Ecl_%5Dny%5B%5D%5D_mmZZZZZZKPQRSKMPNNLNMZZZ%5D; tc_cj_v2_cmp=; tc_cj_v2_med="
        }
        response = requests.get(url, headers=headers)
        results = []
        if response.status_code == 200:
            datas = response.json()
        driver.get("https://www.carrefour.fr/s?q=" + name_product)
        time.sleep(10)

        results = Market.getMarket(
            'Carrefour',
            driver,
            "product-grid-item",
            "product-card-title__text",
            'ds-product-card-refonte__shimzone--medium',
            '.ds-product-card-refonte__perunitlabel',
            'product-price__amount-value',
            "promotion-label-refonte__label",
            '.product-card-image img',
            '.product-card-image'
        )
        sc = StoreCart(name_product, 'Carrefour', results)
        Market.results.append(sc)

    @staticmethod
    def getAuchan(driver, index, name_product, code_postal):
        driver.get("https://www.auchan.fr/recherche?text=" + name_product)
        if index == 0:
            try:
                driver.maximize_window()
                onetrust = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, 'onetrust-accept-btn-handler')))
                driver.execute_script("arguments[0].click();", onetrust)
                thumbnail__see = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                    (By.CLASS_NAME, 'product-thumbnail__see-prices-button')))
                driver.execute_script("arguments[0].click();", thumbnail__see)
                time.sleep(2)
                query = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'journeySearchInput')))
                query.send_keys(code_postal)
                time.sleep(2)
                search_sugest = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'geocoding__search-suggest')))
                driver.execute_script("arguments[0].click();", search_sugest)
                btnJourneySubmit = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, "btnJourneySubmit")))
                driver.execute_script(
                    "arguments[0].click();", btnJourneySubmit)
                driver.minimize_window()
            except TimeoutException:
                File.save_log("Auchan : Loading took too much time !")

        results = Market.getMarket(
            'Auchan',
            driver,
            "product-thumbnail",
            "product-thumbnail__description",
            'product-attribute',
            '.product-thumbnail__attributes span[data-seller-type="GROCERY"]',
            'product-price',
            "product-discount",
            '.product-thumbnail__picture img',
            '.product-thumbnail__details-wrapper'
        )
        sc = StoreCart(name_product, 'Auchan', results)
        Market.results.append(sc)

    @staticmethod
    def getMarket(
        store_name,
        driver,
        products_class,
        name_class,
        description_class,
        price_kg_class,
        price_class,
        discount_class,
        picture_class,
        link_class
    ):
        driver.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(2)
        results = []
        products = driver.find_elements(By.CLASS_NAME, products_class)
        for product in products[:20]:
            name = ''
            description = ''
            price_kg = ''
            price = ''
            discount = ''
            picture = ''
            link = ''

            if name_class:
                try:
                    name = product.find_element(By.CLASS_NAME, name_class).text
                except NoSuchElementException or StaleElementReferenceException:
                    break

            if description_class:
                try:
                    description = product.find_element(
                        By.CLASS_NAME, description_class).text
                except NoSuchElementException or StaleElementReferenceException:
                    pass

            if price_kg_class:
                try:
                    price_kg = product.find_element(
                        By.CSS_SELECTOR, price_kg_class).text
                    if store_name == 'Casino':
                        price_regex = re.findall(
                            r"(?i)\b[0-9]+,[0-9]+\b", price_kg)
                        if len(price_regex) > 0:
                            price_kg = Market.remove_char_price(price_regex[0])
                        else:
                            price_regex = re.findall(
                                r"(?i)\b[0-9]+\b", price_kg)
                            if len(price_regex) > 0:
                                price_kg = Market.remove_char_price(
                                    price_regex[0])
                            else:
                                price_kg = ''
                    else:
                        price_kg = Market.remove_char_price(price_kg)
                except NoSuchElementException or StaleElementReferenceException:
                    pass

            if price_class:
                try:
                    price = product.find_element(
                        By.CLASS_NAME, price_class).text
                    price = Market.remove_char_price(price)
                except NoSuchElementException or StaleElementReferenceException:
                    pass

            if discount_class:
                try:
                    if store_name == 'Casino':
                        discount = product.find_element(
                            By.CSS_SELECTOR, discount_class).get_attribute('src')
                    else:
                        discount = product.find_element(
                            By.CLASS_NAME, discount_class).text
                except NoSuchElementException or StaleElementReferenceException:
                    pass

            if picture_class:
                try:
                    if store_name == 'Aldi':
                        picture = product.find_element(
                            By.CSS_SELECTOR, picture_class).get_attribute('srcset')
                    else:
                        picture = product.find_element(
                            By.CSS_SELECTOR, picture_class).get_attribute('src')
                except NoSuchElementException or StaleElementReferenceException:
                    pass

            if link_class:
                try:
                    link = product.find_element(
                        By.CSS_SELECTOR, link_class).get_attribute('href')
                except NoSuchElementException or StaleElementReferenceException:
                    pass

            p = Product(name, description, price_kg, price,
                        discount, store_name, picture, link)
            results.append(p)
        return results
