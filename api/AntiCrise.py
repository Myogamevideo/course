import requests
from bs4 import BeautifulSoup
import threading
import urllib3
from collections import Counter
from File import File


class AntiCrise:
    unique_name_products = []
    reducs = []
    titles = []

    @staticmethod
    def getReduc(pdts, index, n):
        produits = [pdt.products for pdt in pdts[index]]
        AntiCrise.unique_name_products = []
        AntiCrise.reducs = []

        non_chars = ['00', 'sans', '-', 'a', 'bi', '/', '&', 'de', 'd', 'au', 'avec', 'auchan', 'casino', 'monoprix', 'cora', 'aldi', 'carrefour', 'à', 'la', 'bio',
                     'du', 'et', 'aux', 'en', 'le', 'sur', 'pour', 'pack', 'bouteille', 'boite', 'gr', 'ml', 'uds', 'flash', 'extra', 'petit', ':', 'mini']
        for i in reversed(range(1001)):
            non_chars.extend([str(i), str(i)+'g', str(i) +
                             'l', str(i)+'kg', str(i)+'%'])

        name_products = ' '.join([p.name.lower()
                                 for sublist in produits for p in sublist])
        for char in non_chars:
            name_products = name_products.replace(' '+char+' ', '')
            name_products = name_products.replace(' ('+char+') ', '')
            name_products = name_products.replace(' ('+char, '')
            name_products = name_products.replace(char+') ', '')

        name_products = name_products.split()
        comptes = Counter(name_products)
        name_products = sorted(
            name_products, key=lambda x: comptes[x], reverse=True)
        AntiCrise.unique_name_products = list(set(name_products))[:15]
        File.save_log(AntiCrise.unique_name_products)
        AntiCrise.run()
        return AntiCrise.reducs

    @staticmethod
    def check(string, substrings):
        for substring in substrings:
            if ' '+substring+' ' in string.lower():
                return substring
        return False

    @staticmethod
    def run():
        threads = []
        http = urllib3.PoolManager()

        threads.append(threading.Thread(target=AntiCrise.allOpti))
        threads.append(threading.Thread(target=AntiCrise.getQuoty))
        for name_product in AntiCrise.unique_name_products:
            threads.append(threading.Thread(
                target=AntiCrise.getAntiCrise, args=(name_product,)))
            threads.append(threading.Thread(
                target=AntiCrise.getMaVieEnCouleur, args=(name_product, http)))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

    @staticmethod
    def getAntiCrise(name_product):
        soup = BeautifulSoup(requests.get(
            'https://anti-crise.fr/?s=' + name_product).content, 'html.parser')
        AntiCrise.getData(
            'AntiCrise',
            soup,
            '.post:not(.category-archives):not(.category-tests-de-produits-termines):not(.no-results):not(.category-shopmium-offres-terminees)',
            '.entry-title a',
            '',
            'info'
        )

    @staticmethod
    def getMaVieEnCouleur(name_product, http):
        try:
            resp = http.request(
                "GET",
                'https://www.mavieencouleurs.fr/recherche?search=' +
                name_product + '&univers=All&type=discount_coupon'
            )
            soup = BeautifulSoup(resp.data.decode('utf-8'), 'html.parser')
            AntiCrise.getData(
                'Ma vie en couleurs',
                soup,
                '.views-row',
                '.brand-info span',
                '.coupon-price div',
                ''
            )
        except Exception as e:
            File.save_log(e)
            return

    @staticmethod
    def getQuoty():
        soup = BeautifulSoup(requests.get(
            'https://coupons.quoty.app/fo/coupons?hastolog=true').content, 'html.parser')
        AntiCrise.getData(
            'Quoty',
            soup,
            '.br-item',
            '.description',
            '.br-item-title',
            ''
        )

    @staticmethod
    def getData(
            name_website,
            soup,
            posts_class,
            title_class='',
            price_class='',
            link_class=''
    ):
        posts = soup.select(posts_class)
        for post in posts:
            t = post.select_one(title_class).text.strip(
            ) if title_class != '' and post.select_one(title_class) is not None else ''
            p = post.select_one(price_class).text.strip(
            ) if price_class != '' and post.select_one(price_class) is not None else ''
            link = post.find(class_=link_class).get(
                'href') if link_class != '' and post.find(class_=link_class) is not None else ''
            title = (name_website + ' : ' + t + ' ' + p).lower()
            substring = AntiCrise.check(title, AntiCrise.unique_name_products)
            if substring != False:
                title = title.replace(substring, "[" + substring + "]")
                if title not in AntiCrise.titles:
                    AntiCrise.reducs.append({'title': title, 'link': link})
                    AntiCrise.titles.append(title)

    @staticmethod
    def allOpti():
        try:
            result = []
            soup = BeautifulSoup(requests.get(
                'https://anti-crise.fr/les-catalogues-avec-optimisations/').content, 'html.parser')
            catalogues = soup.select('.catalogue-item')
            for catalogue in catalogues:
                link_catalogue = catalogue.select_one('.info').get('href')
                subsoup = BeautifulSoup(requests.get(
                    link_catalogue).content, 'html.parser')
                name_store = subsoup.select_one('.span8 h1').text.strip()
                tbody = subsoup.select_one('tbody')
                trs = tbody.select('tr')
                for tr in trs:
                    marque = tr.select_one('td:nth-child(3)').text.strip()
                    produit = tr.select_one('td:nth-child(4) a').text.strip()
                    link_produit = link_catalogue + \
                        tr.select_one('td:nth-child(4) a').get('href')
                    quantite = tr.select_one('td:nth-child(5)').text.strip()
                    price = tr.select_one('td:nth-child(6)').text.strip()
                    links = []
                    td_links = tr.select('td:nth-last-child(3) a')
                    for link in td_links:
                        links.append({'title': link.text.strip(),
                                     'link': link.get('href')})

                        final_price = tr.select_one(
                            'td:nth-last-child(2)').text.strip()
                        r = tr.select_one('td:last-child').text.strip()
                    result.append(
                        {
                            'name_store': name_store,
                            'marque': marque,
                            'produit': produit,
                            'link_produit': link_produit,
                            'quantite': quantite,
                            'price': price,
                            'links': links,
                            'final_price': final_price,
                            'r': r,
                        }
                    )
            try:
                sorted_array = sorted(result, key=lambda x: -
                                      1*int(x['r'].strip('%')))
            except ValueError:
                File.save_log("Invalid literal for int() with base 10")
            File.save_simple_file('var/allopti.json', sorted_array)
        except Exception as e:
            File.save_log(e)
            return e
