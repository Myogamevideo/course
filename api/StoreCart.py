import statistics
import functools

class StoreCart:
    def __init__(self, name_product, name_store, products):
        self.name_product = name_product
        self.name_store = name_store
        self.products = sorted(
            products, key=functools.cmp_to_key(StoreCart.compare))
        self.min_prices = self.calculate_minimum_price_per_kg()
        self.avg_prices = self.calculate_average_price_per_kg()
        self.median_prices = self.calculate_median_price_per_kg()
        self.len_products = len(self.products)

    def calculate_len_products(self):
        return len(self.products)

    def calculate_median_price_per_kg(self):
        price_per_kg = [product.price_kg for product in self.products if isinstance(
            product.price_kg, (float, int))]
        if len(price_per_kg) == 0:
            return 0
        return statistics.median(price_per_kg)

    def calculate_average_price_per_kg(self):
        price_per_kg = [product.price_kg for product in self.products if isinstance(
            product.price_kg, (float, int))]
        if len(price_per_kg) == 0:
            return 0
        return sum(price_per_kg) / len(price_per_kg)

    def calculate_minimum_price_per_kg(self):
        price_per_kg = [product.price_kg for product in self.products if isinstance(
            product.price_kg, (float, int))]
        if len(price_per_kg) == 0:
            return 0
        return min(price_per_kg)

    @staticmethod
    def compare(x, y):
        if not isinstance(x, (float, int)) and not isinstance(y, (float, int)):
            x = getattr(x, 'price_kg', x)
            y = getattr(y, 'price_kg', y)

        if isinstance(x, str) or isinstance(y, str) or x is None or y is None:
            if isinstance(x, str) and not isinstance(y, str):
                return 1
            if isinstance(y, str) and not isinstance(x, str):
                return -1
            if x is None and y is not None:
                return 1
            if y is None and x is not None:
                return -1
            return 0

        return x - y
