import getopt
import sys
import time
from selenium import webdriver
import selenium
from Market import Market
from File import File
from AntiCrise import AntiCrise
from ProductCart import ProductCart

class Configuration:
    # Configuration class to store global settings
    code_postal = "63170"
    use_all_opti = False
    log_file = 'var/log/log.txt'
    task_file = '-task.json'
    store_file = '-store.json'
    promo_file = '-promo.json'
    id_file = ''

def handle_error(error):
    # Handle and log errors
    error_message = str(error)
    File.save_log(error_message)
    print(f"An error occurred: {error_message}")


def validate_input(value):
    # Validate user input
    if not value:
        return False
    return True


def process_arguments(argv):
    # Process command line arguments and update configuration accordingly
    try:
        options = 'iop'
        long_options = ['ALLOPTI', 'POSTALCODE', 'IDFILE']
        arguments, values = getopt.getopt(argv, options, long_options)
        for currentArgument, currentValue in arguments:
            if currentArgument in ('-i', "--IDFILE"):
                File.save_log('--IDFILE' + argv[1])
                idfile = argv[1]
                if validate_input(idfile):
                    Configuration.id_file = idfile
                else:
                    raise ValueError("Invalid id file")
                
            if currentArgument in ('-o', "--ALLOPTI"):
                Configuration.use_all_opti = True

            if currentArgument in ('-p', "--POSTALCODE"):
                File.save_log('--POSTALCODE' + argv[3])
                postal = argv[3]
                if validate_input(postal):
                    Configuration.code_postal = postal
                else:
                    raise ValueError("Invalid postal code")

    except (getopt.error, ValueError) as err:
        handle_error(err)


def initialize_webdrivers():
    # Initialize web drivers for each store
    service = selenium.webdriver.chrome.service.Service('/usr/local/bin/chromedriver-linux64/chromedriver')
    options = webdriver.ChromeOptions()
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--disable-popup-blocking")
    options.add_argument('--incognito')
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-gpu')


    drivers = {}
    driver_names = File.read_simple_file(Configuration.id_file + Configuration.store_file)
    for driver_name in driver_names:
        if driver_name not in ['Intermarche', 'CourseU', 'Leclerc']:
            drivers[driver_name] = webdriver.Chrome(options=options, service=service)
        else:
            drivers[driver_name] = None
    return drivers


def run_market(drivers, name_product, index, code_postal):
    # Run the Market class and handle exceptions
    try:
        return Market.run(drivers, name_product.lower(), index, code_postal)
    except Exception as e:
        handle_error(e)
        return None


def run_anticrise(produits, index, name_product):
    # Run the AntiCrise class to get promos and handle exceptions
    try:
        return AntiCrise.getReduc(produits, index, name_product.lower())
    except Exception as e:
        handle_error(e)
        return None


def main(argv=[]):
    process_arguments(argv)
    start = time.time()

    name_products = File.read_simple_file(Configuration.id_file + Configuration.task_file)
    produits = {}
    promos = {}

    drivers = initialize_webdrivers()

    promos_file = File.read_simple_file(Configuration.id_file + Configuration.promo_file)
    for index, name_product in enumerate(name_products):
        sta = time.time()
        promos = None
        File.save_log('-- ' + name_product + ' --')
        produits[index] = run_market(drivers, name_product, index, Configuration.code_postal)
        if promos_file is not None and len(promos_file) > 0:
            st = time.time()
            promos = run_anticrise(produits, index, name_product.lower())
            elapsed_time = time.time() - st
            File.save_log('Execution time: ' + str(elapsed_time) +
                          ' - Promo - ' + name_product)

        productCart = ProductCart(name_product, produits[index], promos)
        elapsed_time = time.time() - sta
        File.save_log('Execution time: ' + str(elapsed_time) + ' - Shop ')
        File.save_products(productCart)

    for driver in drivers.values():
        if driver is not None:
            driver.quit()

    elapsed_time = time.time() - start
    File.save_log('Execution time: ' + str(elapsed_time) + ' - All ')

    if Configuration.use_all_opti:
        File.save_log('--ALLOPTI')
        st = time.time()
        AntiCrise.allOpti()
        elapsed_time = time.time() - st
        File.save_log('Execution time: ' + str(elapsed_time) + ' - allOpti')

    return True


if __name__ == '__main__':
    if len(sys.argv[1:]) > 0:
        main(sys.argv[1:])
    else:
        main([])
