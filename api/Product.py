class Product:
    def __init__(
            self, 
            name, 
            description, 
            price_kg, 
            price, 
            discount, 
            market, 
            picture, 
            link_product
        ):

        self.name = name
        self.price_kg = price_kg
        self.price = price
        self.market = market
        self.description = description
        self.discount = discount
        self.picture = picture
        self.link_product = link_product