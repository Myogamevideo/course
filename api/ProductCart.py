import statistics
from typing import List
from StoreCart import StoreCart
from Product import Product


class ProductCart:
    def __init__(self, name_product: str, products: List[StoreCart], promos: List[str]):
        self.name_product = name_product
        self.list_store_carts = products
        self.products = self.get_all_products(products)
        self.min_prices = self.calculate_minimum_price_per_kg(self.products)
        self.avg_prices = self.calculate_average_price_per_kg(self.products)
        self.median_prices = self.calculate_median_price_per_kg(self.products)
        self.len_products = len(self.products)
        self.promos = promos

    @staticmethod
    def get_all_products(store_carts: List[StoreCart]) -> List[Product]:
        products = []
        for store_cart in store_carts:
            products.extend(store_cart.products)
        return products

    def compare(list: StoreCart):
        products = []
        for store_cart in list:
            products += store_cart.products
        return sorted(products, key=StoreCart.compare)

    @staticmethod
    def calculate_median_price_per_kg(products: List[Product]) -> float:
        price_per_kg = [product.price_kg for product in products if isinstance(
            product.price_kg, (float, int))]
        if not price_per_kg:
            return 0
        return statistics.median(price_per_kg)

    @staticmethod
    def calculate_average_price_per_kg(products: List[Product]) -> float:
        price_per_kg = [product.price_kg for product in products if isinstance(
            product.price_kg, (float, int))]
        if not price_per_kg:
            return 0
        return sum(price_per_kg) / len(price_per_kg)

    @staticmethod
    def calculate_minimum_price_per_kg(products: List[Product]) -> float:
        min_price_per_kg = float('inf')
        for product in products:
            price_kg = product.price_kg
            if isinstance(price_kg, (float, int)) and price_kg < min_price_per_kg:
                min_price_per_kg = price_kg

        return 0 if min_price_per_kg == float('inf') else min_price_per_kg
