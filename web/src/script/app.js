import {verifierDossier} from './result.js'
import {loadData} from './task.js'

// Validation du code postal
function validatePostalCode(postalcode) {
  if (postalcode == ''){
    return true;
  }
  const postalcodePattern = /^\d{5}$/; // Format du code postal (5 chiffres)
  return postalcodePattern.test(postalcode);
}

// Gestion des erreurs améliorée
function displayErrorMessage(message) {
  const errorContainer = document.querySelector('#error-message');
  errorContainer.textContent = message;
}

// Réinitialisation de l'interface utilisateur
function resetInterface() {
  postalcode_input.value = '';
  const errorContainer = document.querySelector('#error-message');
  errorContainer.textContent = '';
}

// Feedback visuel pendant le chargement
function showLoadingIndicator() {
  // Afficher un indicateur de chargement (par exemple, une icône ou une animation)
  // Ajouter/modifier des classes CSS pour afficher l'indicateur de chargement
  body.classList.remove('hide');
}

function hideLoadingIndicator() {
  // Masquer l'indicateur de chargement
  body.classList.add('hide');
}

const loadPricesBtn = document.querySelector('#loadPricesBtn');
const body = document.querySelector('#body');
const postalcode_input = document.querySelector('#postalcode-input');

hideLoadingIndicator();

loadPricesBtn.addEventListener('click', async () => {
  const postalcode_input_value = postalcode_input.value

  if (!validatePostalCode(postalcode_input_value)) {
    displayErrorMessage('Code postal invalide');
    return;
  }

  resetInterface();
  showLoadingIndicator();

  try {
    const response = await fetch('/run-python', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 'postalcode': postalcode_input_value })
    });

    if (!response.ok) {
      throw new Error('Erreur interne du serveur');
    }

    const data = await response.text();
    console.log(data);
    verifierDossier();
  } catch (error) {
    console.error(`Erreur : ${error.message}`);
    displayErrorMessage('Une erreur est survenue. Veuillez réessayer.');
  } finally {
    hideLoadingIndicator();
  }
});

loadData('task');
verifierDossier();

async function allOpti() {
  try {
    const response = await fetch('/run-all-opti');

    if (!response.ok) {
      throw new Error('Erreur interne du serveur');
    }

    const data = await response.json();
    hideLoadingIndicator();

  } catch (error) {
    console.error(`Erreur : ${error.message}`);
    displayErrorMessage('Une erreur est survenue lors de l\'optimisation.');
  }
}

allOpti();