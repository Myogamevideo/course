// Constantes
const tableOpti = document.querySelector('#table-opti');
const tableOptiResume = document.querySelector('#table-opti-resume');
const lenOpti = document.querySelector('#len-opti');
const range = document.querySelector('#range');
const byStore = document.getElementById('byStore');

// Variables
let listeStore = [];
let boolByStore = false;
let valueRange = 100;

// Gestionnaire d'événements pour le bouton "byStore"
byStore.addEventListener('click', function () {
    boolByStore = !boolByStore;
    allOpti();
});

// Gestionnaire d'événements pour la plage de valeurs
range.addEventListener('input', (event) => {
    valueRange = event.target.value;
    listeStore = [];
    allOpti(valueRange);
});

// Fonction de tri des lignes du tableau
function sortRows(tbody, col) {
    const rows = Array.from(tbody.querySelectorAll('tr'));
    const sortedRows = rows.sort((a, b) => {
        const aText = a.cells[col].textContent.trim();
        const bText = b.cells[col].textContent.trim();
        const aVal = parseFloat(aText);
        const bVal = parseFloat(bText);
        if (!isNaN(aVal) && !isNaN(bVal)) {
            return aVal - bVal;
        }
        return aText.localeCompare(bText, 'fr', { sensitivity: 'base' });
    });
    tbody.append(...sortedRows);
}

// Création d'une ligne d'optimisation
function createOptiRow(opti) {
    const tr = document.createElement('tr');

    const tdNameStore = document.createElement('td');
    tdNameStore.textContent = opti.name_store;
    tr.appendChild(tdNameStore);

    const tdMarque = document.createElement('td');
    tdMarque.textContent = opti.marque;
    tr.appendChild(tdMarque);

    const tdProduit = document.createElement('td');
    const a = document.createElement('a');
    a.href = opti.link_produit;
    a.target = '_blank';
    a.classList = 'text-danger';
    a.textContent = opti.produit;
    tdProduit.appendChild(a);
    tr.appendChild(tdProduit);

    const tdQuantite = document.createElement('td');
    tdQuantite.textContent = opti.quantite;
    tr.appendChild(tdQuantite);

    const tdPrice = document.createElement('td');
    tdPrice.textContent = opti.price;
    tr.appendChild(tdPrice);

    const tdFinalPrice = document.createElement('td');
    tdFinalPrice.textContent = opti.final_price;
    tr.appendChild(tdFinalPrice);

    const tdR = document.createElement('td');
    tdR.textContent = opti.r;
    tr.appendChild(tdR);

    const tdLinks = document.createElement('td');
    opti.links.forEach(link => {
        const a = document.createElement('a');
        a.href = link.link;
        a.target = '_blank';
        a.textContent = link.title;
        tdLinks.appendChild(a);
        const span = document.createElement('span');
        span.textContent = ' - ';
        tdLinks.appendChild(span);
    });
    tr.appendChild(tdLinks);

    return tr;
}

// Création du tableau d'optimisations pour un magasin
function createTableOpti(datas, nomStore = '') {
    const table = document.createElement('table');
    table.classList.add('table', 'table-striped');
    let priceTotalLost = 0;
    let numberTotalProduit = 0;
    let priceTotalWin = 0;
    let remboursement = 0;
    let bonus = 0;

    const headers = [
        { text: 'Nom du magasin', key: 'name_store' },
        { text: 'Marque', key: 'marque' },
        { text: 'Produit', key: 'produit' },
        { text: 'Q', key: 'quantite' },
        { text: 'Prix', key: 'price' },
        { text: 'Prix finale', key: 'final_price' },
        { text: 'R', key: 'r' },
        { text: 'Source', key: 'links' }
    ];

    const tbody = document.createElement('tbody');
    datas.forEach(opti => {
        const rValue = parseInt(opti.r.replace('%', ''));
        if (rValue >= valueRange) {
            const storeCartRow = createOptiRow(opti);
            tbody.appendChild(storeCartRow);
            numberTotalProduit++;
            priceTotalLost += parseFloat(opti.price.replace('€', '').replace(',', '.'));
            priceTotalWin += parseFloat(opti.final_price.replace('€', '').replace(',', '.'));
        }
    });

    const thead = document.createElement('thead');
    const tr = document.createElement('tr');
    headers.forEach((column, index) => {
        const th = document.createElement('th');
        th.id = column.key;
        th.textContent = column.text;
        th.addEventListener('click', () => sortRows(tbody, index));
        tr.appendChild(th);
    });

    thead.appendChild(tr);
    table.appendChild(thead);
    table.appendChild(tbody);

    if (numberTotalProduit > 0) {
        if (priceTotalWin < 0) {
            bonus = priceTotalLost + -priceTotalWin;
            remboursement = (priceTotalLost / (priceTotalWin + priceTotalLost)) * 100;
        } else {
            bonus = priceTotalLost - priceTotalWin;
            remboursement = (bonus / priceTotalLost) * 100;
        }

        if (nomStore !== '') {
            const h4 = document.createElement('h4');
            h4.textContent = nomStore;
            tableOpti.appendChild(h4);
            const h6 = document.createElement('h6');
            h6.textContent = `( N°T : ${numberTotalProduit} | T€L : ${priceTotalLost.toFixed(2)} | T€W : ${-priceTotalWin.toFixed(2)} | T€B : ${(bonus < 0 ? -bonus.toFixed(2) : bonus.toFixed(2))} | R : ${remboursement.toFixed(2)}% )`;
            tableOpti.appendChild(h6);
        }

        nomStore = (nomStore === '') ? 'General' : nomStore;
        const store = {
            numberTotalProduit,
            nomStore,
            priceTotalWin,
            bonus,
            remboursement,
            priceTotalLost
        };
        listeStore[nomStore] = store;
    }

    tableOpti.appendChild(table);
}

function createTableResume(datas) {
    const table = document.createElement('table');
    table.classList.add('table', 'table-striped');

    const thead = document.createElement('thead');
    const tr = document.createElement('tr');
    const headers = [
        { text: 'Nom du magasin', key: 'nomStore' },
        { text: 'Nombre total de produit', key: 'numberTotalProduit' },
        { text: 'Argent depensée', key: 'priceTotalLost' },
        { text: 'Argent gagnée ou perdu', key: 'priceTotalWin' },
        { text: 'Bonus finale', key: 'bonus' },
        { text: 'R', key: 'remboursement' }
    ];

    const tbody = document.createElement('tbody');
    for (const prop in datas) {
        const opti = datas[prop];

        const tr = document.createElement('tr');

        const tdNameStore = document.createElement('td');
        tdNameStore.textContent = opti.nomStore;
        tr.appendChild(tdNameStore);

        const tdNumberTotalProduit = document.createElement('td');
        tdNumberTotalProduit.textContent = opti.numberTotalProduit;
        tr.appendChild(tdNumberTotalProduit);

        const tdPriceTotalLost = document.createElement('td');
        tdPriceTotalLost.textContent = opti.priceTotalLost.toFixed(2);
        tr.appendChild(tdPriceTotalLost);

        const tdPrice = document.createElement('td');
        tdPrice.textContent = (-opti.priceTotalWin).toFixed(2);
        tr.appendChild(tdPrice);

        const tdFinalPrice = document.createElement('td');
        tdFinalPrice.textContent = (opti.bonus < 0 ? -opti.bonus.toFixed(2) : opti.bonus.toFixed(2));
        tr.appendChild(tdFinalPrice);

        const tdR = document.createElement('td');
        tdR.textContent = opti.remboursement.toFixed(2) + '%';
        tr.appendChild(tdR);

        tbody.appendChild(tr);
    }

    headers.forEach((column, index) => {
        const th = document.createElement('th');
        th.id = column.key;
        th.textContent = column.text;
        th.addEventListener('click', () => sortRows(tbody, index));
        tr.appendChild(th);
    });

    thead.appendChild(tr);
    table.appendChild(thead);
    table.appendChild(tbody);
    tableOptiResume.appendChild(table);
}

// Fonction pour récupérer toutes les optimisations
async function fetchOptimizations() {
    try {
        const response = await fetch('/run-all-opti');
        if (!response.ok) {
            throw new Error('Erreur interne du serveur');
        }
        return await response.json();
    } catch (error) {
        throw new Error(`Erreur : ${error.message}`);
    }
}

async function allOpti() {
    const optimisation = {};
    try {
        const data = await fetchOptimizations();
        lenOpti.textContent = `(${data.data.length})`;
        const max = parseInt(data.data[0].r.replace('%', ''));
        const min = parseInt(data.data[data.data.length - 1].r.replace('%', ''));
        range.min = min;
        range.max = max;
        tableOpti.textContent = '';
        tableOptiResume.textContent = '';

        if (boolByStore) {
            data.data.forEach(opti => {
                const nameStore = opti.name_store.split('\n')[0];
                optimisation[nameStore] = [];
            });

            data.data.forEach(opti => {
                const nameStore = opti.name_store.split('\n')[0];
                optimisation[nameStore].push(opti);
            });

            for (const opti in optimisation) {
                if (optimisation[opti].length > 0) {
                    createTableOpti(optimisation[opti], opti);
                }
            }
        } else {
            createTableOpti(data.data);
        }

        createTableResume(listeStore);
        
        return data;
    } catch (error) {
        console.error(`Erreur : ${error.message}`);
    }
}

// Initialisation
allOpti();