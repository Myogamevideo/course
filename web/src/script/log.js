const logsDiv = document.getElementById('logs');

function appendMessageToLogs(message) {
  logsDiv.innerHTML += message;
}

io().on('logs', appendMessageToLogs);