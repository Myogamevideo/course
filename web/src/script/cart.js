import {getCart} from './result.js'
const cart = getCart();

const cartList = document.getElementById('cart-list');
const cartResult = document.getElementById('cart-result');

function createTabl(className) {
    const table = document.createElement('table');
    table.classList.add('table', className);
    const tbody = document.createElement('tbody');
    table.appendChild(tbody);
    return { table, tbody };
}

function createTableCell(content, options = {}) {
    const { type = 'text', colspan = 1 } = options;
    const td = document.createElement('td');

    switch (type) {
        case 'image':
            const img = document.createElement('img');
            img.src = content;
            img.classList.add('img-fluid', 'img-table');
            td.appendChild(img);
            break;
        case 'link':
            if (content !== '') {
                const a = document.createElement('a');
                a.textContent = 'Voir le produit';
                a.classList.add('btn', 'btn-success');
                a.target = '_blank';
                a.href = content;
                td.appendChild(a);
            }
            break;
        default:
            td.textContent = content;
    }

    td.setAttribute('colspan', colspan);
    return td;
}

function clearElement(element) {
    element.innerHTML = '';
}

function renderCartList(cart) {
    const tableList = createTabl();
    cartList.innerHTML = '';

    const columns = [
        { id: 'col-img', label: '#' },
        { id: 'col-name', label: 'Nom' },
        { id: 'col-price_kg', label: 'Prix L/KG' },
        { id: 'col-price', label: 'Prix' },
        { id: 'col-description', label: 'Description' },
        { id: 'col-discount', label: 'Promotion' },
        { id: 'col-market', label: 'Magasin' },
        { id: 'col-link_product', label: 'Lien' }
    ];

    cart.forEach(product => {
        if (product.name) {
            const tr = document.createElement('tr');
            const columnsData = [
                { type: 'image', data: product.picture },
                { type: 'text', data: product.name },
                { type: 'text', data: product.price_kg },
                { type: 'text', data: product.price },
                { type: 'text', data: product.description },
                { type: 'imageOrText', data: product.discount },
                { type: 'text', data: product.market },
                { type: 'link', data: product.link_product }
            ];

            columnsData.forEach((columnData, index) => {
                const column = columns[index];
                const td = createTableCell(columnData.data, { type: columnData.type });
                tr.appendChild(td);
            });

            tableList.tbody.appendChild(tr);
        } else {
            const tr = document.createElement('tr');
            const tdName = document.createElement('td');
            tdName.textContent = product.nom;
            const tdData = document.createElement('td');
            tdData.textContent = product.promoName;
            tdData.setAttribute('colspan', '6');
            tr.appendChild(tdName);
            tr.appendChild(tdData);
            tableList.tbody.appendChild(tr);
        }
    });

    cartList.appendChild(tableList.table);
}

function renderCartResult(cart) {
    const tableResult = createTabl();
    cartResult.innerHTML = '';

    const resultat = [
        { label: 'Prix total', data: calculateTotalPrice(cart) },
        { label: 'Prix total au prix au KG/L', data: calculateTotalPricePerKg(cart) }
    ];

    resultat.forEach(r => {
        const tr = document.createElement('tr');
        const tdName = document.createElement('td');
        tdName.textContent = r.label;
        const tdData = document.createElement('td');
        tdData.textContent = r.data + ' €';
        tr.appendChild(tdName);
        tr.appendChild(tdData);
        tableResult.tbody.appendChild(tr);
    });

    const tr = document.createElement('tr');
    const tdButton = document.createElement('td');
    const link = document.createElement('a');
    link.textContent = 'Export';
    link.href = 'var/cart.json';
    link.setAttribute('download', '');
    link.classList = 'btn btn-info';
    tdButton.appendChild(link);

    const mail = document.createElement('a');
    mail.textContent = 'Mail';
    mail.setAttribute('download', '');
    mail.classList = 'btn btn-info';
    mail.addEventListener('click', sendMail);
    tdButton.appendChild(mail);

    tr.appendChild(tdButton);
    tableResult.tbody.appendChild(tr);
    cartResult.appendChild(tableResult.table);
}

function calculateTotalPrice(cart) {
    let totalPrice = 0;
    cart.forEach(product => {
        if (product.price) {
            totalPrice += product.price;
        }
    });
    return totalPrice;
}

function calculateTotalPricePerKg(cart) {
    let totalPrice = 0;
    cart.forEach(product => {
        if (product.price_kg) {
            totalPrice += product.price_kg;
        }
    });
    return totalPrice;
}

async function sendMail() {
    const inputMail = document.querySelector('#email-input').value;
    const data = JSON.stringify({ inputMail });

    try {
        const response = await fetch('/send-mail', {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: data
        });

        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
    } catch (error) {
        console.error('Error sending mail:', error);
    }
}

const tableList = createTabl('table-list');
cartList.appendChild(tableList.table);

const tableResult = createTabl('table-result');
cartResult.appendChild(tableResult.table);

export function changeRowCartList() {
    clearElement(tableList.tbody);
    renderCartList(cart);
    renderCartResult(cart);
}

renderCartList(cart);
renderCartResult(cart);
