import { changeRowCartList } from './cart.js';

const byStore = document.getElementById('byStore');
let boolByStore = false;
let cart = []

byStore.addEventListener('click', function () {
    boolByStore = !boolByStore;
    verifierDossier();
});

const deleteProducts = document.getElementById('deleteProducts');

deleteProducts.addEventListener('click', async function () {
    try {
        const response = await fetch('/product', { method: 'DELETE' });

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            verifierDossier();
        } else {
            throw new Error('Erreur de récupération de suppression');
        }
    } catch (error) {
        console.error(error);
    }
});

function ajouterOnglet(nom, datas, dateModification) {
    nom = nom.split('.')[0];
    nom = nom.replaceAll('+', '-');

    const navPills = document.querySelector('#v-pills-tab');

    const newTabLink = document.createElement('a');
    newTabLink.classList.add('nav-link');
    newTabLink.setAttribute('id', `v-pills-${nom}-tab`);
    newTabLink.setAttribute('data-bs-toggle', 'pill');
    newTabLink.setAttribute('href', `#v-pills-${nom}`);
    newTabLink.setAttribute('role', 'tab');
    newTabLink.setAttribute('aria-controls', `v-pills-${nom}`);
    newTabLink.setAttribute('aria-selected', 'false');
    newTabLink.textContent = `${nom.replaceAll('-', ' ')} - ${dateModification.toLocaleString()}`;

    navPills.appendChild(newTabLink);
    const tabContent = document.querySelector('#v-pills-tabContent');

    const newTabContent = document.createElement('div');
    newTabContent.classList.add('tab-pane', 'fade');
    newTabContent.setAttribute('id', 'v-pills-' + nom);
    newTabContent.setAttribute('role', 'tabpanel');
    newTabContent.setAttribute('aria-labelledby', 'v-pills-' + nom + '-tab');

    newTabContent.appendChild(createTableStat(datas))
    if (datas.promos != null) {
        newTabContent.appendChild(createTablePromo(datas, nom))
    }

    if (boolByStore) {
        for (const { products } of datas.list_store_carts) {
            if (products.length > 0) {
                newTabContent.appendChild(createTable({ products }));
            }
        }
    } else {
        newTabContent.appendChild(createTable(datas))
    }

    tabContent.appendChild(newTabContent);
}

function createTablePromo(datas, nom) {
    const table = document.createElement('table');
    table.classList.add('table', 'table-success', 'table-striped');

    const thead = document.createElement('thead');
    const promo = document.createElement('th');
    promo.textContent = 'PROMO';

    const tbody = document.createElement('tbody');
    datas.promos.forEach(promoName => {
        const tr = document.createElement('tr');
        const td_checkbox = document.createElement('td');
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.classList = 'form-check-input'
        checkbox.dataset.product = JSON.stringify(promoName);
        checkbox.addEventListener('change', () => {
            updateSelectedProductsFile({promoName, nom}, checkbox.checked);
        });
        td_checkbox.appendChild(checkbox);
        tr.appendChild(td_checkbox);
        const td_name = document.createElement('td');
        td_name.textContent = promoName.title;
        tr.appendChild(td_name);
        const td_link = document.createElement('td');
        if (promoName.link !== '') {
            const a = document.createElement('a');
            a.textContent = '🔗';
            a.classList.add('btn', 'btn-success');
            a.target = '_blank';
            a.href = promoName.link;
            td_link.appendChild(a);
        }
        tr.appendChild(td_link);
        tbody.appendChild(tr);
    });

    thead.appendChild(promo);
    table.appendChild(thead);
    table.appendChild(tbody);

    return table;
}

function createTableStat(datas) {
    const table_responsive = document.createElement('div');
    table_responsive.classList = 'table-responsive'
    const table = document.createElement('table');
    table.classList.add('table', 'table-success', 'table-striped');

    const thead = document.createElement('thead');
    const tr = document.createElement('tr');
    const headers = [{ text: 'Nom', key: 'name_store' }, { text: 'Nombre de produit', key: 'len_products' }, { text: 'MIN', key: 'min_prices' }, { text: 'AVG', key: 'avg_prices' }, { text: 'MEDIAN', key: 'median_prices' }];
    headers.forEach(header => {
        const th = document.createElement('th');
        th.textContent = header.text;
        tr.appendChild(th);
    });

    thead.appendChild(tr);

    const tbody = document.createElement('tbody');
    const globalRow = createStatRow('Global', datas.len_products, datas.min_prices, datas.avg_prices, datas.median_prices);
    tbody.appendChild(globalRow);

    datas.list_store_carts.forEach(storeCart => {
        const storeCartRow = createStatRow(storeCart.name_store, storeCart.len_products, storeCart.min_prices, storeCart.avg_prices, storeCart.median_prices);
        tbody.appendChild(storeCartRow);
    });

    table.appendChild(thead);
    table.appendChild(tbody);

    table_responsive.appendChild(table)
    return table_responsive;
}

function createStatRow(name, len_products, min_prices, avg_prices, median_prices) {
    const tr = document.createElement('tr');

    const td_name = document.createElement('td');
    td_name.textContent = name;
    tr.appendChild(td_name);

    const td_len_products = document.createElement('td');
    td_len_products.textContent = len_products;
    tr.appendChild(td_len_products);

    const td_min_prices = document.createElement('td');
    td_min_prices.textContent = min_prices;
    tr.appendChild(td_min_prices);

    const td_avg_prices = document.createElement('td');
    td_avg_prices.textContent = avg_prices;
    tr.appendChild(td_avg_prices);

    const td_median_prices = document.createElement('td');
    td_median_prices.textContent = median_prices;
    tr.appendChild(td_median_prices);

    return tr;
}

function createTable(datas) {
    const table_responsive = document.createElement('div');
    table_responsive.classList = 'table-responsive'
    const table = document.createElement('table');
    table.classList.add('table', 'table-success', 'table-striped');

    const thead = document.createElement('thead');
    const tr = document.createElement('tr');
    thead.appendChild(tr);

    const columns = [{ id: 'col-select', label: '' }, { id: 'col-img', label: '#' }, { id: 'col-name', label: 'Nom' }, { id: 'col-price_kg', label: 'Prix L/KG' }, { id: 'col-price', label: 'Prix' }, { id: 'col-description', label: 'Description' }, { id: 'col-discount', label: 'Promotion' }, { id: 'col-market', label: 'Magasin' }, { id: 'col-link_product', label: 'Lien' }];

    const tbody = document.createElement('tbody');
    datas.products.forEach(product => {
        const tr = document.createElement('tr');
        const columnsData = [{ type: 'checkbox', data: product }, { type: 'image', data: product.picture }, { type: 'text', data: product.name }, { type: 'text', data: product.price_kg }, { type: 'text', data: product.price }, { type: 'text', data: product.description }, { type: 'imageOrText', data: product.discount }, { type: 'text', data: product.market }, { type: 'link', data: product.link_product }];

        columns.forEach((column, index) => {
            const td = document.createElement('td');
            const columnData = columnsData[index];
            switch (columnData.type) {
                case 'checkbox':
                    const checkbox = document.createElement('input');
                    checkbox.type = 'checkbox';
                    checkbox.classList = 'form-check-input'
                    checkbox.dataset.product = JSON.stringify(columnData.data);
                    checkbox.addEventListener('change', () => {
                        updateSelectedProductsFile(columnData.data, checkbox.checked);
                    });
                    td.appendChild(checkbox);
                    break;
                case 'image':
                    const img = document.createElement('img');
                    img.src = columnData.data;
                    img.classList.add('img-fluid', 'img-table');
                    td.appendChild(img);
                    break;
                case 'text':
                    td.textContent = columnData.data;
                    break;
                case 'imageOrText':
                    if (columnData.data != null && columnData.data.includes('https')) {
                        const img = document.createElement('img');
                        img.src = columnData.data;
                        img.classList.add('img-fluid', 'img-table');
                        td.appendChild(img);
                    } else {
                        td.textContent = columnData.data;
                    }
                    break;
                case 'link':
                    if (columnData.data !== '') {
                        const a = document.createElement('a');
                        a.textContent = '🔗';
                        a.classList.add('btn', 'btn-success');
                        a.target = '_blank';
                        a.href = columnData.data;
                        td.appendChild(a);
                    }
                    break;
                default:
                    break;
            }
            tr.appendChild(td);
        });
        tbody.appendChild(tr);
    });

    columns.forEach((column, index) => {
        const th = document.createElement('th');
        th.id = column.id;
        th.textContent = column.label;
        th.addEventListener('click', () => sortTable(tbody, index));
        tr.appendChild(th);
    });

    table.appendChild(thead);
    table.appendChild(tbody);

    function sortTable(tbody, col) {
        const rows = Array.from(tbody.querySelectorAll('tr'));
        const sortedRows = rows.sort((a, b) => {
            const aText = a.cells[col].textContent.trim();
            const bText = b.cells[col].textContent.trim();
            const aVal = parseFloat(aText);
            const bVal = parseFloat(bText);
            if (!isNaN(aVal) && !isNaN(bVal)) {
                return aVal - bVal;
            }
            return aText.localeCompare(bText, 'fr', { sensitivity: 'base' });
        });
        tbody.append(...sortedRows);
    }

    table_responsive.appendChild(table)
    return table_responsive;
}

function updateSelectedProductsFile(selectedProduct, checked) {
    if (checked) {
        cart.push(selectedProduct)
    } else {
        var index = cart.indexOf(selectedProduct);
        if (index !== -1) {
            cart.splice(index, 1);
        }
    }

    const datas = JSON.stringify(cart);
    fetch('/save-cart', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: datas
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return text = response.text()
        })
        .then(text => {
            console.log(text);
        })
        .catch(error => {
            console.error('Error saving selected products:', error);
        });

    changeRowCartList()
}

export async function verifierDossier() {
    try {
        const response = await fetch('/product');

        if (!response.ok) {
            throw new Error('Erreur de récupération des données');
        }

        const datas = await response.json();
        const tab = document.getElementById('v-pills-tab');
        const tabContent = document.getElementById('v-pills-tabContent');

        tab.innerHTML = '';
        tabContent.innerHTML = '';

        if (datas.length > 0) {
            datas.sort((a, b) => new Date(b.dateModification) - new Date(a.dateModification));

            for (const fichier of datas) {
                ajouterOnglet(fichier.nom, fichier.contenu, new Date(fichier.dateModification));
            }
        }
    } catch (error) {
        console.error(error);
    }
}

export function getCart(){
    return cart;
}