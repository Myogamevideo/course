const stores = ['Auchan', 'Monoprix', 'Cora', 'Aldi', 'Carrefour', 'Intermarche', 'CourseU', 'Leclerc'];
const stores_online = ['AntiCrise', 'Intermarche', 'CourseU', 'Leclerc'];
const stores_map = ['Auchan', 'Intermarche', 'CourseU', 'Leclerc'];
const promos = ['AntiCrise'];

const storesDiv = document.getElementById('stores');
const promosDiv = document.getElementById('promos');
const tasksList = document.getElementById('taskList');
const taskInput = document.getElementById('taskInput');
const addTaskBtn = document.getElementById('addTaskBtn');

let tasks = [];

taskInput.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        addTask();
    }
});

async function saveData(url, selectedData) {
    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(selectedData)
        });
        
        if (!response.ok) {
            throw new Error('Unable to save data.');
        }
    } catch (error) {
        console.error(error);
    }
}

function renderTasks() {
    tasksList.innerHTML = '';
    tasks.forEach((task, index) => {
        const tr = document.createElement('tr');
        const td = document.createElement('td');
        td.textContent = task.replaceAll("+", " ");

        const tdb = document.createElement('td');
        const deleteButton = document.createElement('button')
        deleteButton.classList = 'btn btn-outline-danger';
        deleteButton.textContent = 'Supprimer de la liste';
        deleteButton.addEventListener('click', () => deleteTask(index));
        tdb.appendChild(deleteButton);

        tr.appendChild(td);
        tr.appendChild(tdb);
        tasksList.appendChild(tr);
    });
}

export async function loadData(url) {
    try {
        if(document.cookie.split('; ').find(c => c.startsWith(url + '=')) != undefined){
            tasks = JSON.parse(decodeURIComponent(document.cookie.split('; ').find(c => c.startsWith(url + '=')).split('=')[1]));
        }
        renderTasks();
    } catch (error) {
        console.error(error);
    }
}

function addTask() {
    const newTaskTitle = taskInput.value.trim();
    if (newTaskTitle === '') {
        alert('Please enter a task title.');
        return;
    }
    tasks.push(newTaskTitle);
    taskInput.value = '';
    saveData('/save-task', tasks.map(task => task.replaceAll(" ", "+")));
    renderTasks();
}

function deleteTask(index) {
    tasks.splice(index, 1);
    saveData('/save-task', tasks.map(task => task.replaceAll(" ", "+")));
    renderTasks();
}

async function getData(url, data, div, callback) {
    try {
        let json = undefined;
        if(document.cookie.split('; ').find(c => c.startsWith(url + '=')) != undefined){
            json = JSON.parse(decodeURIComponent(document.cookie.split('; ').find(c => c.startsWith(url + '=')).split('=')[1]));
        }
        data.forEach((item) => {
            const checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.id = item;
            checkbox.classList.add("form-check-input");
            div.appendChild(checkbox);

            const label = document.createElement('label');
            label.htmlFor = item;
            label.textContent = item;
            label.classList.add("form-check-label");

            const isOnline = stores_online.includes(item);
            const isMapped = stores_map.includes(item);

            const span = document.createElement('span');
            span.classList = "mx-1 badge bg-secondary";
            span.textContent = isOnline ? 'request' : 'selenium';
            label.appendChild(span);

            if (isMapped) {
                const span = document.createElement('span');
                span.classList = "mx-1 badge bg-info";
                span.textContent = 'with postal code';
                label.appendChild(span);
            }

            div.appendChild(label);
            div.appendChild(document.createElement('br'));

            if(json){
                checkbox.checked = json.includes(item);
            }
        });

        callback();
    } catch (error) {
        console.error(error);
    }
}

function listenCheckboxChanges() {
    const stores = document.querySelectorAll('#stores input[type="checkbox"]');
    const promos = document.querySelectorAll('#promos input[type="checkbox"]');

    stores.forEach((store) => {
        store.addEventListener('change', () => {
            const selectedStores = Array.from(stores)
                .filter(store => store.checked)
                .map(store => store.id);

            saveData('/save-store', selectedStores);
        });
    });

    promos.forEach((promo) => {
        promo.addEventListener('change', () => {
            const selectedPromos = Array.from(promos)
                .filter(promo => promo.checked)
                .map(promo => promo.id);

            saveData('/save-promo', selectedPromos);
        });
    });
}

getData('store', stores, storesDiv, listenCheckboxChanges);
getData('promo', promos, promosDiv, listenCheckboxChanges);
loadData('task');

addTaskBtn.addEventListener('click', addTask);
