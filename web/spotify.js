const express = require('express');
const axios = require('axios');
const SpotifyWebApi = require('spotify-web-api-node');
const ChromecastAPI = require('chromecast-api');
const { log } = require('./socket');

const router = express.Router();

const spotifyApi = new SpotifyWebApi({
    clientId: '93e836ce05594fd38e916c7279c57e0e',
    clientSecret: '919f10582079460badb898f65d13e7e2',
    //redirectUri: 'http://192.168.0.50:3000/spotify/callback'
    redirectUri: 'http://localhost:3000/spotify/callback'
});

let deviceName = 'Connect TV de SFR';
let intervalID = 0;
let oldMediaUrl = '';
const searchCache = new Map();

const scopes = [
    'user-read-private',
    'user-read-email',
    'user-read-currently-playing',
    'playlist-modify-public',
    'playlist-modify-private'
];

async function playOnChromecast(mediaURL) {
    if (!mediaURL) {
        throw new Error('Le paramètre mediaURL est requis');
    }
    const client = new ChromecastAPI();
    return new Promise((resolve, reject) => {
        client.on('device', function (device) {
            log(device.friendlyName);
            if (device.friendlyName === deviceName && mediaURL !== oldMediaUrl) {
                device.play(mediaURL, function (err) {
                    if (!err) {
                        log('Playing on your Chromecast');
                        oldMediaUrl = mediaURL;
                        resolve();
                    } else {
                        log('Erreur lors de la lecture sur Chromecast');
                        log(err);
                        reject(err);
                    }
                });
            } else {
                resolve();
            }
        });
    });
}

async function getCurrentlyPlayingTrack() {
    try {
        const data = await spotifyApi.getMyCurrentPlayingTrack();
        if (data.body.item) {
            const track = data.body.item;
            const titre = track.name;
            const artiste = track.artists[0].name;
            log(`La musique en écoute sur Spotify est ${artiste} - ${titre}`);
            const mediaURL = await searchVideo(artiste, titre, 'watch');
            await playOnChromecast(mediaURL);
        } else {
            log('Il n\'y a pas de résultat');
        }
    } catch (err) {
        log('Erreur lors de la récupération de la musique en cours de lecture');
        log(err);
    }
}

async function getCurrentlyPlayingTrackURL() {
    try {
        if(spotifyApi.getAccessToken()){
            const data = await spotifyApi.getMyCurrentPlayingTrack();
            if (data.body.item) {
                const track = data.body.item;
                const titre = track.name;
                const artiste = track.artists[0].name;
                log(`La musique en écoute sur Spotify est ${artiste} - ${titre}`);
                return await searchVideo(artiste, titre);
            } else {
                log('Il n\'y a pas de résultat');
            }  
        } else {
            log('Il n\'y a pas de token')
        }
    } catch (err) {
        log('Erreur lors de la récupération de la musique en cours de lecture');
        log(err);
        throw err;
    }
}

async function searchVideo(artiste, titre, watch = '') {
    if (!artiste || !titre) {
        throw new Error('Les paramètres artiste et titre sont requis');
    }

    const searchKey = `${artiste}-${titre}`;
    if (searchCache.has(searchKey)) {
        return searchCache.get(searchKey);
    }

    const search = artiste + ' - ' + titre;
    try {
        const response = await axios.get(`https://www.youtube.com/results?search_query=${search}`);
        const data = response.data;
        const regex = /"videoIds":\["(.*?)"\]/g;
        const videoIds = [];
        let match;
        while ((match = regex.exec(data)) !== null) {
            videoIds.push(match[1]);
        }
        if (videoIds.length > 0) {
            const videoUrl = watch === 'watch'
                ? 'https://www.youtube.com/watch?v=' + videoIds[0]
                : 'https://www.youtube.com/embed/' + videoIds[0];
            log(`La première vidéo pour ${artiste} - ${titre} est : ${videoUrl}`);
            searchCache.set(searchKey, videoUrl);
            return videoUrl;
        } else {
            throw new Error('Aucune vidéo trouvée');
        }
    } catch (error) {
        log('Erreur lors de la recherche de la vidéo');
        log(error);
        return '';
    }
}

router.post('/chromecast', (req, res) => {
    let { deviceName } = req.body;
    if (!deviceName) {
        return res.status(400).json({ error: 'Le paramètre deviceName est requis' });
    }

    deviceName = req.body.deviceName;
    return res.status(200).json({ deviceName });
});

router.get('/authorization', (req, res) => {
    const authorizeURL = spotifyApi.createAuthorizeURL(scopes, 'some-state-of-my-choice');
    return res.status(200).json({ url: authorizeURL });
});

router.get('/callback', async (req, res) => {
    const code = req.query.code;

    try {
        const data = await spotifyApi.authorizationCodeGrant(code);
        log('The token expires in ' + data.body['expires_in']);
        log('The access token is ' + data.body['access_token']);
        log('The refresh token is ' + data.body['refresh_token']);
        spotifyApi.setAccessToken(data.body['access_token']);
        spotifyApi.setRefreshToken(data.body['refresh_token']);
        return res.status(200).send();
    } catch (err) {
        log('Something went wrong!', err);
        return res.status(500).send();
    }
});

router.get('/', (req, res) => {
    res.sendFile(__dirname + '/src/spotify.html');
});

router.get('/clip', async (req, res) => {
    const mediaURL = await getCurrentlyPlayingTrackURL();
    return res.status(200).json({ url: mediaURL });
});

router.get('/run-spotify', (req, res) => {
    getCurrentlyPlayingTrack();
    intervalID = setInterval(getCurrentlyPlayingTrack, 10000);
    return res.status(200).json({ data: 'Interval started' });
});

router.get('/stop-spotify', (req, res) => {
    clearInterval(intervalID);
    log('Interval stopped');
    return res.status(200).json({ data: 'Interval stopped' });
});

router.get('/access-token', (req, res) => {
    const accessToken = spotifyApi.getAccessToken();
    if (accessToken) {
        return res.status(200).json({ access_token: accessToken });
    } else {
        return res.status(404).json({ access_token: '' });
    }
});

module.exports = router