const socketIO = require('socket.io');
const fs = require('fs');
const winston = require('winston');
const express = require('express');
const logRoutes = express.Router();

let io;
const date = new Date();
const dateStringYYYYMMDD = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
const logger = winston.createLogger({
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'var/log/['+dateStringYYYYMMDD+']-log.log' })
    ]
});

function socket(server, app) {
    io = socketIO(server);
    io.on('connection', (socket) => {
        log('Client connected');
        fs.readFile('var/log/['+dateStringYYYYMMDD+']-log.log', 'utf8', (err, data) => {
            if (err) {
                logger.error(err); 
                return;
            }
            socket.emit('logs', data);
        });
    });
}

function getIO() {
    if (!io) {
        throw new Error('Socket.IO not initialized!');
    }
    return io;
}

function log(message) {
    logger.info(message);
    io = getIO();
    io.emit('logs', message);
}

logRoutes.get('/', (req, res) => {
    res.sendFile(__dirname + '/src/logs.html');
});

module.exports = { socket, log, logRoutes };