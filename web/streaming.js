const express = require('express');
const axios = require('axios');
const cheerio = require('cheerio');

const router = express.Router();

const streamings = {
    disney: 'https://www.leparisien.fr/guide-shopping/disney-plus-quels-sont-les-series-et-les-films-a-voir-10-04-2021-8430988.php',
    canal: 'https://www.leparisien.fr/guide-shopping/canal-plus-quels-sont-les-series-et-les-films-a-voir-07-04-2021-8430777.php',
    paramout: 'https://www.leparisien.fr/guide-shopping/paramount-quels-sont-les-films-et-series-a-voir-11-01-2023-WSZVETAQ2BGHXMN64CXWC2W55U.php',
    amazon: 'https://www.leparisien.fr/guide-shopping/prime-video-quels-sont-les-films-et-series-a-voir-19-03-2021-8429155.php',
    ocs: 'https://www.leparisien.fr/guide-shopping/ocs-quels-sont-les-series-et-les-films-a-voir-10-04-2021-8430989.php',
    netflix: 'https://www.leparisien.fr/guide-shopping/netflix-quels-sont-les-series-et-les-films-a-voir-22-03-2021-8429439.php'
};

const titlesAvailables = [
    'séries', 'films', 'disponible', 'documentaires', 'émissions'
];

const searchCache = new Map();

async function searchIMDB(search) {
    const response = await axios.get(`https://www.imdb.com/find/?q=${search}&ref_=nv_sr_sm`);
    const $ = cheerio.load(response.data);
    const element = $('.find-title-result').eq(0).find('a').text()
    console.log(element);
}

async function fetchData(streamingService, url) {
    const response = await axios.get(url);
    const $ = cheerio.load(response.data);
    const contents = $('.content');
    let type = '';
    const createFilmPromises = [];

    for (const element of contents) {
        let title = $(element).find('.inline_title').text().trim();
        let nextElement = $(element).next();
        let summary = nextElement.find('.paragraph').text().trim();
        let date = nextElement.find('b').text().trim().replace('Date de sortie : ', '').replace('.', '');

        for (const titleAvailable of titlesAvailables) {
            if (title.includes(titleAvailable)) {
                type = titleAvailable;
            }
        };

        if (title && nextElement.is('.content') && summary) {
            createFilmPromises.push(createFilm(title, summary, date, streamingService, type));
        }
    };

    const films = await Promise.all(createFilmPromises);
    return films;
}

async function createFilm(title, summary, date, streamingService, type) {
    imdb = await searchIMDB(title);
    urlVideo = await searchVideo(streamingService, title);
    return { title, summary, date, streamingService, type, urlVideo };
}

async function searchVideo(artiste, titre, watch = '') {
    if (!artiste || !titre) {
        throw new Error('Les paramètres artiste et titre sont requis');
    }

    const searchKey = `${artiste}-${titre}`;
    if (searchCache.has(searchKey)) {
        return searchCache.get(searchKey);
    }

    const search = artiste + ' - ' + titre;
    try {
        const response = await axios.get(`https://www.youtube.com/results?search_query=${search}`);
        const data = response.data;
        const regex = /"videoIds":\["(.*?)"\]/g;
        const videoIds = [];
        let match;
        while ((match = regex.exec(data)) !== null) {
            videoIds.push(match[1]);
        }
        if (videoIds.length > 0) {
            const videoUrl = watch === 'watch'
                ? 'https://www.youtube.com/watch?v=' + videoIds[0]
                : 'https://www.youtube.com/embed/' + videoIds[0];
            searchCache.set(searchKey, videoUrl);
            return videoUrl;
        } else {
            throw new Error('Aucune vidéo trouvée');
        }
    } catch (error) {
        console.log('Erreur lors de la recherche de la vidéo');
        console.log(error);
        return '';
    }
}

async function getMovies() {
    let films = [];

    for (const [streamingService, url] of Object.entries(streamings)) {
        const data = await fetchData(streamingService, url);
        films.push(...data);
    }

    return films;
}


router.get('/', async (req, res) => {
    try {
        const movies = await getMovies();
        return res.status(200).json({ data: movies });
    } catch (error) {
        console.log('Erreur lors de la récupération des films');
        console.log(error);
        return res.status(500).json({ error: 'Erreur lors de la récupération des films' });
    }
});

module.exports = router