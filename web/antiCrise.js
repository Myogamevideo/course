const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

class AntiCrise {
    static unique_name_products = [];
    static reducs = [];
    static titles = [];

    static async getReduc(pdts, index, n) {
        const produits = pdts[index].map((pdt) => pdt.products);
        AntiCrise.unique_name_products = [];
        AntiCrise.reducs = [];

        const non_chars = [
            '00', 'sans', '-', 'a', 'bi', '/', '&', 'de', 'd', 'au', 'avec', 'auchan', 'casino', 'monoprix', 'cora', 'aldi', 'carrefour', 'à', 'la', 'bio',
            'du', 'et', 'aux', 'en', 'le', 'sur', 'pour', 'pack', 'bouteille', 'boite', 'gr', 'ml', 'uds', 'flash', 'extra', 'petit', ':', 'mini'
        ];
        for (let i = 1000; i >= 0; i--) {
            non_chars.push(String(i), String(i) + 'g', String(i) + 'l', String(i) + 'kg', String(i) + '%');
        }

        let name_products = produits.flat().map((p) => p.name.toLowerCase()).join(' ');

        for (const char of non_chars) {
            name_products = name_products.replace(' ' + char + ' ', '');
            name_products = name_products.replace(' (' + char + ') ', '');
            name_products = name_products.replace(' (' + char, '');
            name_products = name_products.replace(char + ') ', '');
        }

        const name_products_array = name_products.split(' ');
        const comptes = name_products_array.reduce((counter, name) => {
            counter[name] = (counter[name] || 0) + 1;
            return counter;
        }, {});

        const sorted_name_products = name_products_array.sort((a, b) => comptes[b] - comptes[a]);
        AntiCrise.unique_name_products = Array.from(new Set(sorted_name_products)).slice(0, 15);
        AntiCrise.run();
        return AntiCrise.reducs;
    }

    static check(string, substrings) {
        for (const substring of substrings) {
            if (string.toLowerCase().includes(' ' + substring + ' ')) {
                return substring;
            }
        }
        return false;
    }

    static run() {
        const requests = [];
        requests.push(AntiCrise.allOpti());
        requests.push(AntiCrise.getQuoty());
        for (const name_product of AntiCrise.unique_name_products) {
            requests.push(AntiCrise.getAntiCrise(name_product));
            requests.push(AntiCrise.getMaVieEnCouleur(name_product));
        }
        return Promise.all(requests);
    }

    static async getAntiCrise(name_product) {
        try {
            const response = await axios.get('https://anti-crise.fr/?s=' + name_product);
            const $ = cheerio.load(response.data);
            AntiCrise.getData(
                'AntiCrise',
                $,
                '.post:not(.category-archives):not(.category-tests-de-produits-termines):not(.no-results):not(.category-shopmium-offres-terminees)',
                '.entry-title a',
                '',
                'info'
            );
        } catch (error) {
            console.error(error);
        }
    }

    static async getMaVieEnCouleur(name_product) {
        try {
            const response = await axios.get(
                'https://www.mavieencouleurs.fr/recherche?search=' + name_product + '&univers=All&type=discount_coupon'
            );
            const $ = cheerio.load(response.data);
            AntiCrise.getData(
                'Ma vie en couleurs',
                $,
                '.views-row',
                '.brand-info span',
                '.coupon-price div',
                ''
            );
        } catch (error) {
            console.error(error);
        }
    }

    static async getQuoty() {
        try {
            const response = await axios.get('https://coupons.quoty.app/fo/coupons?hastolog=true');
            const $ = cheerio.load(response.data);
            AntiCrise.getData(
                'Quoty',
                $,
                '.br-item',
                '.description',
                '.br-item-title',
                ''
            );
        } catch (error) {
            console.error(error);
        }
    }

    static getData(name_website, $, posts_class, title_class = '', price_class = '', link_class = '') {
        const posts = $(posts_class);
        posts.each((index, post) => {
            const title = (name_website + ' : ' + $(post).find(title_class).text().trim() + ' ' + $(post).find(price_class).text().trim()).toLowerCase();
            const substring = AntiCrise.check(title, AntiCrise.unique_name_products);
            if (substring) {
                const formattedTitle = title.replace(substring, '[' + substring + ']');
                if (!AntiCrise.titles.includes(formattedTitle)) {
                    AntiCrise.reducs.push({ title: formattedTitle, link: $(post).find(link_class).attr('href') });
                    AntiCrise.titles.push(formattedTitle);
                }
            }
        });
    }

    static async allOpti() {
        try {
            const result = [];
            const response = await axios.get('https://anti-crise.fr/les-catalogues-avec-optimisations/');
            const $ = cheerio.load(response.data);
            const catalogues = $('.catalogue-item');
            for (let i = 0; i < catalogues.length; i++) {
                const link_catalogue = $(catalogues[i]).find('.info').attr('href');
                const response = await axios.get(link_catalogue);
                const subsoup = cheerio.load(response.data);
                const name_store = subsoup('.span8 h1').text().trim();
                const tbody = subsoup('tbody');
                const trs = tbody.find('tr');
                trs.each((index, tr) => {
                    const marque = subsoup(tr).find('td:nth-child(3)').text().trim();
                    const produit = subsoup(tr).find('td:nth-child(4) a').text().trim();
                    const link_produit = link_catalogue + subsoup(tr).find('td:nth-child(4) a').attr('href');
                    const quantite = subsoup(tr).find('td:nth-child(5)').text().trim();
                    const price = subsoup(tr).find('td:nth-child(6)').text().trim();
                    const links = [];
                    const td_links = subsoup(tr).find('td:nth-last-child(3) a');
                    td_links.each((index, link) => {
                        links.push({ title: subsoup(link).text().trim(), link: subsoup(link).attr('href') });
                    });
                    const final_price = subsoup(tr).find('td:nth-last-child(2)').text().trim();
                    const r = subsoup(tr).find('td:last-child').text().trim();
                    result.push({
                        name_store,
                        marque,
                        produit,
                        link_produit,
                        quantite,
                        price,
                        links,
                        final_price,
                        r
                    });
                });
            }
            try {
                const sorted_array = result.sort((a, b) => parseInt(b.r.trim('%'), 10) - parseInt(a.r.trim('%'), 10));
                fs.writeFileSync('var/allopti.json', JSON.stringify(sorted_array));
                return JSON.stringify(sorted_array);
            } catch (error) {
                console.error('Invalid literal for int() with base 10');
            }
        } catch (error) {
            console.error(error);
        }
    }
}

module.exports = AntiCrise;
