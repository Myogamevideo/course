const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const { exec } = require('child_process');
const fs = require('fs');
const nodemailer = require('nodemailer');
const { v4: uuidv4 } = require('uuid');

// APP
const app = express();
const port = 3000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'web/src')));

// Server setup
const server = app.listen(port, () => {
    console.log(`Serveur en cours d'exécution sur le port ${port}`);
});

// LOG
const { socket, log, logRoutes } = require('./web/socket.js');
socket(server);
app.use('/logs', logRoutes);

// SPOTIFY
const spotifyRoutes = require('./web/spotify.js');
app.use('/spotify', spotifyRoutes);

// STREAMING
const streamingRoutes = require('./web/streaming.js');
app.use('/streaming', streamingRoutes);

//ANTICRISE
const AntiCrise = require('./web/antiCrise.js');

/**
 * Executes a Python command.
 * @param {string} command - The command to execute.
 * @returns {Promise<string>} - A promise resolving to the command output.
 */
const runPythonCommand = (command) => {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                console.error(`Erreur d'exécution de la commande : ${error}`);
                reject(new Error('Erreur interne du serveur'));
            }
            log(`stdout: ${stdout}`);
            log(`stderr: ${stderr}`);
            resolve('Commande Python exécutée avec succès');
        });
    });
};

function getcookie(req, url) {
    var cookie = req.headers.cookie;
    let data = [];
    if(cookie.split('; ').find(c => c.startsWith(url + '=')) != undefined){
        data = JSON.parse(decodeURIComponent(cookie.split('; ').find(c => c.startsWith(url + '=')).split('=')[1]));
    }
    return data;
}

/**
 * Runs a Python route.
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 */
app.post('/run-python', async (req, res) => {
    log('run-python');
    const postalCode = req.body.postalcode || '';
    log(postalCode);

    try {
        const id = uuidv4();
        saveFile(id + '-task', getcookie(req, 'task'))
        saveFile(id + '-store', getcookie(req, 'store'))
        saveFile(id + '-promo', getcookie(req, 'promo'))

        const command = postalCode === '' ? `python3 api/main.py -i ${id}` : `python3 api/main.py -i ${id} -o ${postalCode}`;
        await runPythonCommand(command);

        deleteFile(id + '-task')
        deleteFile(id + '-store')
        deleteFile(id + '-promo')

        res.status(200).send('Commande Python exécutée avec succès');
    } catch (error) {
        console.error(error);
        res.status(500).send('Erreur interne du serveur');
    }
});

const deleteFile = (filename) => {
    const filePath = `var/cookie/${filename}.json`;
    fs.unlink(filePath, err => {
        if (err) {
            console.error(`Erreur lors de la suppresion du fichier ${filename}.json : ${err}`);
        }
        log(`Fichier supprimé avec succès : ${filename}.json`);
    });
}

/**
 * Saves file content to a specified file.
 * @param {string} filename - The name of the file to save.
 * @returns {Function} - The middleware function for saving the file.
 */
const saveFile = (filename, data) => {
    const filePath = `var/cookie/${filename}.json`;
    const fileContent = JSON.stringify(data, null, 4);
    fs.writeFile(filePath, fileContent, (err) => {
        if (err) {
            console.error(`Erreur lors de l'écriture dans le fichier ${filename}.json : ${err}`);
        }
        log(`Données sauvegardées dans le fichier ${filename}.json`);
    });
};

const saveCookie = (cookieName) => (req, res) => {
    const cookieData = JSON.stringify(req.body, null, 4);
    const options = {
      maxAge: 1000 * 60 * 60 * 24 * 7, // 7 jours en millisecondes
      httpOnly: false, // Empêche l'accès au cookie via JavaScript
      secure: false, // Envoie le cookie uniquement sur HTTPS
    };
    res.cookie(cookieName, cookieData, options);
    log(`Données sauvegardées dans le cookie ${cookieName}`);
    return res.status(200).send(`Données sauvegardées dans le cookie ${cookieName}`);
};

app.post('/save-task', saveCookie('task'));
app.post('/save-store', saveCookie('store'));
app.post('/save-promo', saveCookie('promo'));
app.post('/save-cart', saveCookie('cart'));

/**
 * Reads files from a specified folder.
 * @param {string} folderPath - The path of the folder to read files from.
 * @returns {Function} - The middleware function for reading the files.
 */
const readFiles = (folderPath) => (req, res) => {
    if (!fs.existsSync(folderPath)) {
        try {
            fs.mkdirSync(folderPath);
        } catch (error) {
            console.error(`Erreur lors de la création du dossier ${folderPath}: ${error}`);
            res.status(500).send('Erreur interne du serveur');
            return;
        }
    }

    fs.readdir(folderPath, (err, files) => {
        if (err) {
            console.error(`Erreur de lecture du dossier ${folderPath} : ${err}`);
            res.status(500).send('Erreur interne du serveur');
        }
        const data = files.map((filename) => {
            const filePath = path.join(folderPath, filename);
            const fileContent = fs.readFileSync(filePath, 'utf8');
            const fileStats = fs.statSync(filePath);
            return {
                nom: filename,
                contenu: JSON.parse(fileContent),
                dateModification: fileStats.mtime,
            };
        });
        res.status(200).json(data);
    });
};

app.get('/product', readFiles('var/product/'));

/**
 * Removes all files from a specified folder.
 * @param {string} folderPath - The path of the folder to remove files from.
 * @returns {Promise<string>} - A promise resolving to the result message.
 */
function removeFiles(folderPath) {
    return new Promise((resolve, reject) => {
        fs.rm(folderPath, { recursive: true }, (err) => {
            if (err) {
                reject(new Error('Error deleting files'));
            } else {
                resolve('All files in folder have been deleted.');
            }
        });
    });
}

app.delete('/product', async (req, res) => {
    try {
        const result = await removeFiles('var/product/');
        res.status(200).json({ message: result });
    } catch (error) {
        console.error(error);
        res.status(500).send('Erreur interne du serveur');
    }
});

/**
 * Sends an email with the cart data.
 * @param {string} recipient - The email recipient.
 * @returns {Promise<string>} - A promise resolving to the result message.
 */
const sendEmail = (recipient) => {
    return new Promise((resolve, reject) => {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'alexandre.bougrat@gmail.com',
                pass: 'rlpzqzcojekrkcxc'
            }
        });

        const mailOptions = {
            from: 'alexandre.bougrat@gmail.com',
            to: recipient,
            subject: 'Ma liste de course',
            text: fs.readFileSync('var/cart.json', 'utf8'),
            attachments: [
                {
                    filename: 'cart.json',
                    path: 'var/cart.json'
                }
            ]
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                reject(new Error(`Failed to send email: ${error}`));
            } else {
                resolve(`Email envoyé: ${info.response}`);
            }
        });
    });
}

app.post('/send-mail', async (req, res) => {
    try {
        const result = await sendEmail(req.body.inputMail);
        log(result);
        res.status(200).send(result);
    } catch (error) {
        console.error(error);
        res.status(500).send('Erreur interne du serveur');
    }
});

/**
 * Runs all optimization and returns the result.
 * @returns {Promise<Object>} - A promise resolving to the optimization result.
 */

const runAllOptimization = () => {
    return new Promise((resolve, reject) => {
        const allOptiFilePath = 'var/allopti.json';

        const executePythonOptiCommand = () => {
            AntiCrise.allOpti();
            fs.readFile(allOptiFilePath, (err, data) => {
                if (err) {
                    log(err);
                    console.error(err);
                    reject(new Error('Erreur interne du serveur'));
                } else {
                    resolve({ data: JSON.parse(data) });
                }
            });
        };

        fs.access(allOptiFilePath, fs.constants.F_OK, (e) => {
            if (e) {
                log('new-opti');
                executePythonOptiCommand();
            } else {
                fs.stat(allOptiFilePath, (err, stats) => {
                    if (err) {
                        console.error(err);
                        reject(new Error('Erreur interne du serveur'));
                    }

                    const lastModified = new Date(stats.atime);
                    const yesterday = new Date(Date.now() - 6 * 60 * 60 * 1000);

                    executePythonOptiCommand();

                    if (lastModified < yesterday) {
                        log('new-opti : ' + lastModified);
                        executePythonOptiCommand();
                    } else {
                        log('with-file : ' + lastModified);
                        fs.readFile(allOptiFilePath, 'utf8', (err, data) => {
                            if (err) {
                                console.error(err);
                                reject(new Error('Erreur interne du serveur'));
                            } else {
                                resolve({ data: JSON.parse(data) });
                            }
                        });
                    }
                });
            }

            log('File exists');
        });
    });
}

app.get('/run-all-opti', async (req, res) => {
    log('run-all-opti')
    try {
        const result = await runAllOptimization();
        res.status(200).json(result);
    } catch (error) {
        console.error(error);
        res.status(500).send('Erreur interne du serveur');
    }
});